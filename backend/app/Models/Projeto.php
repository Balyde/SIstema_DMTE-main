<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Projeto extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $fillable = [
        'tipo',
        'resumo',
        'palavras_chave',
        'periodo_inicio',
        'periodo_finalizacao',
    ];
}
