<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Monografia extends Model
{
    use HasFactory;

    protected $primaryKey = "id";

    protected $fillable = [
        "titulo",
        "anodefesa",
        "file",
        "orientador",
        "campus",
        "palavras_chave",
        "resumo",
        "orientando",
        "linhasdepesquisa",
    ];
}
