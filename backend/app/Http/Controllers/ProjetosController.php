<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Projeto;

class ProjetosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projetos = Projeto::orderBy('id', 'ASC')->get();
        return $projetos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tipo' => 'required',
            'resumo' => 'required',
            'palavras_chave' => 'sometimes',
            'periodo_inicio' => 'required|date',
            'periodo_finalizacao' => 'required|date',
        ]);

        return Projeto::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $projeto = Projeto::find($id);

        if ($projeto) {
            return response()->json(['status' => true, 'projeto' => $projeto]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $projeto = Projeto::findOrFail($id);
        
        $this->validate($request, [
            'tipo' => 'sometimes',
            'resumo' => 'sometimes',
            'palavras_chave' => 'sometimes',
            'periodo_inicio' => 'sometimes|date',
            'periodo_finalizacao' => 'sometimes|date',
        ]);

        $projeto->update($request->all());

        return ['message' => 'Registro atualizado'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projeto = Projeto::findOrFail($id);
        $projeto->delete();

        return Projeto::all();
    }
}
