<?php

namespace App\Http\Controllers;

use App\Models\Mestrando;
use Illuminate\Http\Request;

class MestrandoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mestrando  $mestrando
     * @return \Illuminate\Http\Response
     */
    public function show(Mestrando $mestrando)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mestrando  $mestrando
     * @return \Illuminate\Http\Response
     */
    public function edit(Mestrando $mestrando)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mestrando  $mestrando
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mestrando $mestrando)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mestrando  $mestrando
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mestrando $mestrando)
    {
        //
    }
}
