<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadController extends Controller
{
    public function uploadPDF(Request $request) {
        if ($request->hasFile('pdf')) {
            $file = $request->file('pdf');
            $filename = time() . '_' . $file->getClientOriginalName();
            $file->storeAs('pdfs', $filename, 'public'); // Salvar o arquivo na pasta 'storage/app/public/pdfs'
            
            return response()->json(['message' => 'Arquivo PDF enviado com sucesso!']);
        }
        
        return response()->json(['error' => 'Nenhum arquivo foi enviado.'], 400);
    }
}
