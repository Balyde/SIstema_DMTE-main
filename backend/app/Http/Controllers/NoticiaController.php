<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Noticia;
use Illuminate\Support\Facades\Storage;

class NoticiaController extends Controller{

    public function index()
    {
        $Noticia= Noticia::orderBy('id','ASC')->get();
        return $Noticia;
    }

    // public function store(Request $request)
    // {
    //     // Validação dos campos do formulário
    //     $request->validate([
    //         'titulo' => 'required|string',
    //         'texto' => 'required|string',
    //         'file' => 'sometimes|image|mimes:jpeg,png,jpg,gif|max:2048', // Validação específica para imagens
    //     ]);
    
    //     $pasta = 'public/imagem/';
    //     $url = 'fotos';
    //     $imagens = '';
    
    //     // Verifica se o diretório existe ou cria-o
    //     if (!\File::isDirectory($pasta . $url)) {
    //         \File::makeDirectory($pasta . $url, 0777, true, true);
    //     }
    
    //     if ($request->hasFile('file')) {
    //         $uploadedFile = $request->file('file');
    //         $name = time() . '.' . $uploadedFile->getClientOriginalExtension();
    //         $uploadedFile->storeAs($url, $name);
    //         $imagens = $url . '/' . $name;
    //     }
    
    //     $noticia = new Noticia([
    //         'titulo' => $request->input('titulo'),
    //         'texto' => $request->input('texto'),
    //         'file' => $imagens,
    //     ]);
    
    //     $noticia->save();
    
    //     // Retornar uma resposta apropriada após a criação bem-sucedida da Noticia
    //     return response()->json(['message' => 'Notícia criada com sucesso']);
    // }

    public function store(Request $request)
    {
        $this->validate($request,
        [
            'texto'=> 'sometimes',
            'titulo'=> 'sometimes',
            'file'=> 'sometimes',	
        ]);
        $pasta = 'public/noticias/';
        $url = 'imagens';
        $imagens = '';

        \File::isDirectory($pasta.$url) or \File::makeDirectory($pasta.$url, 0777, true, true);       
        if(substr($request->file,0,10)=="data:image"){               
            $name = time().'.' . explode('/', explode(':', substr($request->file, 0, strpos($request->file, ';')))[1])[1];
            $teste = \Image::make($request->file)->save($pasta.$url.'/'.$name);
            $imagens = $url.'/'.$name;                                                     
            $request->merge(['file'=>$imagens]);
            //return $teste;
        }
    return Noticia::create($request->all());
    }

    


    public function UltimasNoticias()
    {
        $UltimasNoticias = Noticia::orderBy('id', 'desc')->limit(4)->get();
        return $UltimasNoticias;
    }
    
}
