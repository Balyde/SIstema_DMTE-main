<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Monografia;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class MonografiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monografias = Monografia::all();
        return $monografias;
    }

    public function getMonografiaPDF($fileId) {

        $monografia = monografia::find($fileId);
        $filePath = Storage::path("public\\pdfs\\" . $monografia->path);
        $filePath = $filePath."$monografia->file";
        //return $filePath;
        return response()->file($filePath);
    }
    

    public function createAndUpload(Request $request)
    {
    $filename='';
    $this->validate($request, [
        'titulo' => 'sometimes|string',
        'file' ,
        'orientador' => 'sometimes|string',
        'campus' => 'sometimes|string',
        'palavras_chave' => 'sometimes|string',
        'linhasdepesquisa' => 'sometimes|string',
        'resumo' => 'sometimes|string',
        'orientando' => 'sometimes|string',
        'anodefesa' => 'sometimes|date',
    ], [
        'file.required' => 'Campo obrigatório',
        'file.mimes' => 'O arquivo deve ser do tipo PDF',
        'file.max' => 'O tamanho máximo do arquivo é 2MB',
    ]);

    if ($request->hasFile('pdf')) {
        $file = $request->file('pdf');
        $filename = 'pdfs/' . time() . '_' . $file->getClientOriginalName();
        $filePath = public_path($filename); // Get the full path to the file
    
        $file->storeAs('pdfs', $filename, 'public'); // Save the file to the 'storage/app/public/pdfs' directory
    
        // Now you can use the $filePath variable to get the full URL to the uploaded file
        $fullUrl = url($filename);
    }

    $monografia = new Monografia([
        'titulo' => $request->input('titulo'),
        'orientador' => $request->input('orientador'),
        'campus' => $request->input('campus'),
        'palavras_chave' => $request->input('palavras_chave'),
        'resumo' => $request->input('resumo'),
        'orientando' => $request->input('orientando'),
        'file' =>$filename,
        'linhasdepesquisa' => $request->input('linhasdepesquisa'),
        'anodefesa' => $request->input('anodefesa'),
    ]);

    $monografia->save();

    return response()->json(['message' => 'Monografia e arquivo PDF enviados com sucesso!']);
}

public function deleteMonografia($id)
{
    // Encontra a monografia pelo ID
    $monografia = Monografia::find($id);
    $file = Monografia::where('id', $id)->pluck('file')->first();
    
    // Verifica se a monografia existe
    if (!$monografia) {
        return response()->json(['message' => 'Monografia não encontrada.'], 404);
    }

    // Define o caminho do arquivo PDF
    $caminhoArquivo = storage_path('app\\public\\pdfs\\pdfs\\' . $file . '.pdf');
    return $caminhoArquivo;
    // Verifique se o arquivo existe antes de excluir
    if (file_exists($caminhoArquivo)) {
        unlink($caminhoArquivo); // Use unlink para excluir o arquivo
    } else {
        return response()->json(['message' => 'Arquivo não encontrado'], 404);
    }

    // Deleta a monografia do banco de dados
    $monografia->delete();

    return response()->json(['message' => 'Monografia e arquivo PDF excluídos com sucesso.']);
}



}
