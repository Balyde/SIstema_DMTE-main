<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Monografia;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;



class PDFController extends Controller
{
    public function downloadPDF(Request $request)
    {
        $filePath = parse_url($request->input('file_path'), PHP_URL_PATH);

        if (!Storage::exists($filePath)) {
            abort(404);
        }

        $fileContent = Storage::get($filePath);

        $response = Response::make($fileContent, 200);
        $response->header('Content-Type', 'application/pdf');
        $response->header('Content-Disposition', 'inline; filename="' . basename($filePath) . '"');

        return $response;
    }
}