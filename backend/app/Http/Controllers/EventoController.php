<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Evento;
use Illuminate\Support\Facades\Storage;


class EventoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Evento= Evento::orderBy('id','ASC')->get();
        return $Evento;
    }



    public function get_image()
    {
    $eventos = Evento::orderBy('id', 'ASC')->get();

    $data = [];

    foreach ($eventos as $evento) {
        // Concatenar o caminho e o nome do arquivo
        $file = "C:\\xampp\\htdocs\\Sistema_DMTE-main\\backend\\public\\imagem\\" . $evento->file . ".png";
        
        // Adicionar o caminho completo ao array de dados
        $data[] = $file;
    }

    return response()->json($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    public function getEventoPNG($fileId)
{
    
    $evento = Evento::find($fileId); // Substituí "monografia" por "evento" para corresponder ao seu modelo.
    
    if ($evento) {
        $filePath = public_path("imagem" . $evento->path . '\\' . $evento->file);
        // return $filePath;
        if (file_exists($filePath)) {
            return response()->file($filePath);
        } else {
            return response()->json(['message' => 'Arquivo não encontrado'], 404);
        }
    } else {
        return response()->json(['message' => 'Evento não encontrado'], 404);
    }
}


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [
            'name'=> 'sometimes',
            'professor'=> 'sometimes',
            'Campus'=> 'sometimes',
            'Link'=> 'sometimes',
            'file'=> 'sometimes',	
        ]);
        $pasta = 'public/imagem/';
        $url = 'fotos';
        $imagens = '';

        \File::isDirectory($pasta.$url) or \File::makeDirectory($pasta.$url, 0777, true, true);       
        if(substr($request->file,0,10)=="data:image"){               
            $name = time().'.' . explode('/', explode(':', substr($request->file, 0, strpos($request->file, ';')))[1])[1];
            $teste = \Image::make($request->file)->save($pasta.$url.'/'.$name);
            $imagens = $url.'/'.$name;                                                     
            $request->merge(['file'=>$imagens]);
            //return $teste;
        }
    return Evento::create($request->all());
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Evento=Evento::find($id);
        if($Evento){
            return response()->json(['status'=>true,'Evento'=>$Evento]);
        }else{
            return response()->json(['status'=>false]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Evento = Evento::findOrFail($id);
        $this->validate($request,[
            'name'=> 'required',
            'fk_professor'=> 'sometimes',
            'Data'=> 'sometimes',
            'Campus'=> 'sometimes',
            'Link'=> 'sometimes',
            'Tipo'=> 'sometimes',	
        ]);
        $Evento->update($request->all());
        return ['message' => 'Registro atualizado']; 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Evento = Evento::findOrFail($id);
        
        // Obtém o nome do arquivo do evento
        $nomeArquivo = $Evento->file;
        
        // Constrói o caminho completo para o arquivo
        $caminhoArquivo = public_path('imagem/') . $nomeArquivo;
        
        // Verifica se o arquivo existe antes de tentar excluí-lo
        if (file_exists($caminhoArquivo)) {
            // Exclui o arquivo
            unlink($caminhoArquivo);
        }
        
        // Deleta o registro do evento
        $Evento->delete();
        
        // Retorna todos os registros restantes da tabela "Evento"
        return Evento::all();
    }
    


    public function ultimosEventos()
    {
        $ultimosEventos = Evento::orderBy('data', 'desc')->limit(3)->get();
        
        return $ultimosEventos;
    }
}
