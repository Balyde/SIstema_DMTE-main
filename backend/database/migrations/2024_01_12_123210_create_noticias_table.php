<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiasTable extends Migration
{
    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->id();
            $table->string('titulo', 255)->nullable();
            $table->text('texto')->nullable();
            $table->string('file', 255)->nullable();
            $table->timestamps(0); // O argumento 0 remove a precisão de milissegundos nos campos de data e hora
        });
    }

    public function down()
    {
        Schema::dropIfExists('noticias');
    }
    
};
