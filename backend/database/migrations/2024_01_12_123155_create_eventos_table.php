<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventosTable extends Migration
{
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->id(); 
            $table->string('name', 50)->nullable();
            $table->dateTime('event_date')->nullable();
            $table->string('Campus', 50)->nullable();
            $table->string('Link', 250)->nullable();
            $table->timestamps(); // Isso cria as colunas 'created_at' e 'updated_at'
            $table->string('professor', 100)->nullable();
            $table->tinyInteger('tipo')->nullable();
            $table->string('file', 255)->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('eventos');
    }


}
