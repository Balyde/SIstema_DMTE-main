<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUserstable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191)->charset('utf8mb4')->collation('utf8mb4_unicode_ci');
            $table->string('email', 191)->charset('utf8mb4')->collation('utf8mb4_unicode_ci')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 191)->charset('utf8mb4')->collation('utf8mb4_unicode_ci');
            $table->string('remember_token', 100)->charset('utf8mb4')->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('username', 100)->charset('utf8mb4')->collation('utf8mb4_unicode_ci')->nullable();
            $table->integer('perfil');
            $table->text('foto')->charset('utf8mb4')->collation('utf8mb4_unicode_ci')->nullable();
            $table->integer('type')->nullable();
            $table->timestamps(0);

        });

        DB::table('users')->insert([
            [
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => '$2y$10$kRkG0BJixbIlTeFsiIQfMeKPmCQgqlZL5ZnJ.3TCCJTZ4/Ie.Z6Ri',
                'created_at' => '2023-02-14 16:55:38',
                'updated_at' => '2023-02-14 16:55:38',
                'username' => 'admin',
                'perfil' => 1,
            ],

        ]);
    }
    //teste

    public function down()
    {
        Schema::dropIfExists('users');
    }
};
