<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Createperfilpermissaotable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfil_permissoes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('perfil_id');
            $table->unsignedBigInteger('permissao_id');
            $table->timestamps();

            $table->foreign('perfil_id')->references('id')->on('perfil');
            $table->foreign('permissao_id')->references('id')->on('permissoes');
        });

        DB::table('perfil_permissoes')->insert([
            ['id' => 1, 'perfil_id' => 1, 'permissao_id' => 1, 'created_at' => '2023-02-14 17:09']
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('perfil_permissoes');
    }
}
