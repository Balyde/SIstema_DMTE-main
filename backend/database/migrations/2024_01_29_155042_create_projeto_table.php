<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjetoTable extends Migration
{
    public function up()
    {
        Schema::create('projetos', function (Blueprint $table) {
            $table->id();
            $table->integer('tipo');
            $table->string('resumo', 255);
            $table->string('palavras_chave', 255);
            $table->timestamp('periodo_inicio');
            $table->timestamp('periodo_finalizacao')->default(now()); // Set default to current timestamp
            $table->string('titulo');
            $table->string('coordenador', 100);
            $table->string('linhas_pesquisa', 100);
            $table->timestamps(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('projetos');
    }
}

