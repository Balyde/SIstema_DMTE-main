<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfisTable extends Migration
{
    public function up()
    {
        Schema::create('perfil', function (Blueprint $table) {
            $table->id();
            $table->string('perfil', 20)->charset('utf8mb4')->collation('utf8mb4_unicode_ci');
            $table->timestamps(0);
        });

        
        DB::table('perfil')->insert([
            [
                'id' => 1,
                'perfil' => 'teste1',
                'created_at' => '2023-02-13 23:01:29',
                'updated_at' => '2023-02-13 23:01:29'
            ],
            [
                'id' => 2,
                'perfil' => 'teste2',
                'created_at' => '2023-08-09 15:03:33',
                'updated_at' => '2023-08-09 15:03:33'
            ]
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('perfil');
    }
}
