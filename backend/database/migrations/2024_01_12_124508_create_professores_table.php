<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessoresTable extends Migration
{
    public function up()
    {
    Schema::create('professores', function (Blueprint $table) {
        $table->id();
        $table->string('name', 50);
        $table->string('Email', 100)->nullable();
        $table->string('Email_Institucional', 100)->nullable();
        $table->string('Lattes', 100)->nullable();
        $table->timestamps(0); // O argumento 0 remove a precisão de milissegundos nos campos de data e hora
        $table->integer('titulacao', false, true)->nullable();
    });
    }
    
    public function down()
    {
        Schema::dropIfExists('professores');
    }
};
