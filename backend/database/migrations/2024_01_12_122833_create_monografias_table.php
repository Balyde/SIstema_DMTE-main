<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonografiasTable extends Migration
{
    public function up()
    {
        Schema::create('monografias', function (Blueprint $table) {
            $table->id(); // Chave primária auto-incremento
            $table->string('titulo', 250)->nullable();
            $table->text('file')->nullable();
            $table->timestamps(); // Colunas 'created_at' e 'updated_at'
            $table->string('orientador', 255)->nullable();
            $table->string('campus', 255)->nullable();
            $table->text('palavras_chave')->nullable();
            $table->text('resumo')->nullable();
            $table->string('orientando', 255)->nullable();
            $table->string('linhasdepesquisa', 100)->nullable();
            $table->date('anodefesa')->nullable();
        });
    }
    

    public function down()
    {
        Schema::dropIfExists('monografias');
    }
}
