<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissoes', function (Blueprint $table) {
            $table->id();
            $table->string('permissao', 50)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('texto', 50);
        });

        DB::table('permissoes')->insert([
            'id' => 1,
            'permissao' => 'teste',
            'created_at' => '2023-02-14 17:08:18',
            'updated_at' => '2023-02-14 17:08:18',
            'texto' => 'teste',
        ]);

    }
    public function down()
    {
        Schema::dropIfExists('permissoes');
    }
}
