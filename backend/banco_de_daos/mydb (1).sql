-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 03/10/2023 às 23:50
-- Versão do servidor: 10.4.28-MariaDB
-- Versão do PHP: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `mydb`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `alunos`
--

CREATE TABLE `alunos` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `emprestimomonografia`
--

CREATE TABLE `emprestimomonografia` (
  `id` int(11) NOT NULL,
  `NomeSolicitante` varchar(50) DEFAULT NULL,
  `Email` varchar(250) DEFAULT NULL,
  `fk_monografia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `emprestimos`
--

CREATE TABLE `emprestimos` (
  `id` int(11) NOT NULL,
  `fk_equipamento` int(11) NOT NULL,
  `Data` datetime DEFAULT NULL,
  `Devolucao` datetime DEFAULT NULL,
  `LocalUso` varchar(250) DEFAULT NULL,
  `EmailSolicitante` varchar(250) DEFAULT NULL,
  `fk_user` int(7) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='	';

--
-- Despejando dados para a tabela `emprestimos`
--

INSERT INTO `emprestimos` (`id`, `fk_equipamento`, `Data`, `Devolucao`, `LocalUso`, `EmailSolicitante`, `fk_user`, `created_at`, `updated_at`) VALUES
(1, 2, '2023-02-22 11:27:00', '2023-02-22 11:27:00', 'teste', 'teste@gmail.com', 1, '2023-02-22 17:36:43', '2023-02-22 17:36:43'),
(2, 1, '2023-02-22 11:46:00', '2023-02-22 11:46:00', 'teste', 'teste@gmail.com', 1, '2023-02-22 17:47:00', '2023-02-22 17:47:00'),
(3, 9, '2023-06-01 18:56:00', '2023-06-01 18:56:00', 'auditorio', 'lucas@gmail.com', 1, '2023-06-02 00:57:05', '2023-06-02 00:57:05');

-- --------------------------------------------------------

--
-- Estrutura para tabela `equipamentos`
--

CREATE TABLE `equipamentos` (
  `id` int(11) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `estado` int(2) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `modelo` varchar(50) DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `file` tinytext DEFAULT NULL,
  `marca` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Despejando dados para a tabela `equipamentos`
--

INSERT INTO `equipamentos` (`id`, `descricao`, `estado`, `name`, `modelo`, `valor`, `file`, `marca`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'luva', NULL, NULL, NULL, 'teste', '2023-02-16 17:21:27', '2023-08-05 14:49:45'),
(2, 'teste', NULL, 'fio', 'teste', 100, NULL, 'teste', '2023-02-22 17:14:22', '2023-08-08 17:34:12'),
(4, 'teste', 2, 'placa de video', 'teste', 100, NULL, 'teste', '2023-02-22 17:53:29', '2023-08-07 16:01:24'),
(5, 'teste', 2, 'teste', 'teste', 100, NULL, 'teste', '2023-02-22 17:53:33', '2023-02-22 17:53:33'),
(6, NULL, NULL, 'teclado', NULL, NULL, NULL, 'tgjrm', '2023-02-22 19:14:42', '2023-08-06 14:28:45'),
(7, NULL, NULL, 'mouse', NULL, NULL, NULL, 'ijnigdjbd', '2023-02-22 19:34:53', '2023-08-05 14:42:13'),
(9, 'dawuhdiu', 1, 'wdwaiud', 'dwdiuahd', 1, NULL, 'diuwadau', '2023-06-02 00:51:30', '2023-06-02 00:51:30'),
(10, 'testew', 1, 'notebook', 'persona', 1, NULL, 'lenovo', '2023-06-05 15:35:16', '2023-06-05 15:35:16'),
(11, 'teste', NULL, 'monitor', '1', 120, NULL, 'teste', '2023-08-05 15:11:22', '2023-08-05 15:11:22'),
(12, NULL, NULL, 'teste', NULL, NULL, NULL, 'BBB', '2023-08-06 15:49:44', '2023-08-07 15:59:37'),
(13, 'teste', NULL, 'teste', 'teste', 100, NULL, 'teste', '2023-08-19 15:18:46', '2023-08-19 15:18:46');

-- --------------------------------------------------------

--
-- Estrutura para tabela `eventos`
--

CREATE TABLE `eventos` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Campus` varchar(50) DEFAULT NULL,
  `Link` varchar(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `professor` varchar(100) DEFAULT NULL,
  `tipo` int(1) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Despejando dados para a tabela `eventos`
--

INSERT INTO `eventos` (`id`, `name`, `Data`, `Campus`, `Link`, `created_at`, `updated_at`, `professor`, `tipo`, `file`) VALUES
(12, 'oficina', '2023-08-08 14:12:06', 'teste3', 'teste3', '2023-08-08 20:08:18', '2023-08-08 20:08:18', 'Felipe', NULL, '1695148097.jpeg'),
(17, 'reunião', '2023-08-17 00:00:00', 'teste', 'tetse', '2023-08-17 21:34:03', '2023-08-17 21:34:03', 'Matheus', NULL, '1694382672.png'),
(25, 'teste', '2023-08-19 00:00:00', 'teste', 'teste', '2023-08-19 16:24:58', '2023-08-19 16:24:58', '1', NULL, '1694382672.png'),
(26, 'teste', '2023-08-20 11:03:00', 'teste', 'teste', '2023-08-20 14:03:41', '2023-08-20 14:03:41', NULL, 1, '1695148097.jpeg'),
(29, 'teste', '2023-08-20 11:28:00', 'teste', 'teste', '2023-08-20 14:28:28', '2023-08-20 14:28:28', NULL, 1, '1694382672.png'),
(30, 'teste', '2023-08-20 11:32:00', 'teste', 'teste', '2023-08-20 14:33:12', '2023-08-20 14:33:12', NULL, 1, '1695148097.jpeg'),
(31, 'teste', '2023-08-20 11:37:00', 'teste', 'teste', '2023-08-20 14:38:00', '2023-08-20 14:38:00', NULL, 1, '1694382672.png'),
(32, 'teste', '2023-08-20 11:41:00', 'teste', 'teste', '2023-08-20 14:41:47', '2023-08-20 14:41:47', NULL, 1, '1695148097.jpeg'),
(33, NULL, '2023-08-20 12:06:00', 'teste', 'teste', '2023-08-20 15:07:35', '2023-08-20 15:07:35', NULL, 1, '1694382672.png'),
(34, NULL, '2023-08-20 12:09:00', 'teste', 'teste', '2023-08-20 15:09:59', '2023-08-20 15:09:59', NULL, 2, '1695148097.jpeg'),
(35, NULL, '2023-08-20 12:19:00', 'teste', 'teste', '2023-08-20 15:19:56', '2023-08-20 15:19:56', NULL, 2, '1694382672.png'),
(36, NULL, '2023-08-20 12:21:00', 'teste', 'teste', '2023-08-20 15:31:43', '2023-08-20 15:31:43', NULL, 2, '1695148097.jpeg'),
(37, NULL, '2023-08-20 12:51:00', 'teste', 'teste', '2023-08-20 15:51:57', '2023-08-20 15:51:57', NULL, 3, '1694382672.png'),
(38, NULL, '2023-08-20 12:55:00', 'teste', 'teste', '2023-08-20 15:56:34', '2023-08-20 15:56:34', NULL, 4, '1695148097.jpeg'),
(39, NULL, '2023-08-20 12:59:00', 'teste', 'teste', '2023-08-20 16:00:34', '2023-08-20 16:00:34', NULL, 2, '1694382672.png'),
(40, '1', '2023-09-06 15:00:00', '1', '1', '2023-08-30 18:01:13', '2023-08-30 18:01:13', NULL, 1, '1695148097.jpeg'),
(41, NULL, NULL, NULL, NULL, '2023-08-30 19:10:18', '2023-08-30 19:10:18', NULL, NULL, '1694382672.png'),
(42, NULL, NULL, NULL, NULL, '2023-08-30 19:12:11', '2023-08-30 19:12:11', NULL, NULL, '1695148097.jpeg'),
(43, NULL, NULL, NULL, NULL, '2023-08-30 19:12:54', '2023-08-30 19:12:54', NULL, NULL, '1694382672.png'),
(44, '1', '2023-08-29 16:13:00', '1', '1', '2023-08-30 19:13:22', '2023-08-30 19:13:22', NULL, 1, '1695148097.jpeg'),
(45, '1', '2023-09-06 16:14:00', '1', '1', '2023-08-30 19:14:26', '2023-08-30 19:14:26', NULL, 1, '1694382672.png'),
(46, 'teste', '2023-09-01 18:07:00', '1', '11', '2023-09-01 21:31:04', '2023-09-01 21:31:04', NULL, 1, '1695148097.jpeg'),
(47, '1', '2023-09-01 18:53:00', '1', '1', '2023-09-01 21:53:43', '2023-09-01 21:53:43', NULL, 1, '1694382672.png'),
(48, '1', '2023-09-01 18:56:00', '1', '1', '2023-09-01 21:56:16', '2023-09-01 21:56:16', NULL, 1, '1695148097.jpeg'),
(49, '1', '2023-09-01 19:03:00', '1', '1', '2023-09-01 22:03:39', '2023-09-01 22:03:39', NULL, 1, '1694382672.png'),
(50, '3', '2023-09-01 19:06:00', '3', '3', '2023-09-01 22:06:34', '2023-09-01 22:06:34', NULL, 3, '1695148097.jpeg'),
(51, NULL, NULL, NULL, NULL, '2023-09-01 22:07:54', '2023-09-01 22:07:54', NULL, NULL, '1694382672.png'),
(52, NULL, NULL, NULL, NULL, '2023-09-01 22:08:16', '2023-09-01 22:08:16', NULL, NULL, '1695148097.jpeg'),
(53, '3', '2023-09-09 19:09:00', '3', '3', '2023-09-01 22:10:09', '2023-09-01 22:10:09', NULL, 3, '1694382672.png'),
(54, 'teste', '2023-09-09 11:19:00', '1', '1', '2023-09-09 14:26:00', '2023-09-09 14:26:00', NULL, 1, '1695148097.jpeg'),
(55, '1', '2023-09-09 11:33:00', '1', '1', '2023-09-09 14:34:34', '2023-09-09 14:34:34', NULL, 1, '1694382672.png'),
(56, '1', '2023-09-09 11:41:00', '1', '1', '2023-09-09 14:42:05', '2023-09-09 14:42:05', NULL, 1, '1695148097.jpeg'),
(57, '1', '2023-09-10 18:37:00', '1', '1', '2023-09-10 21:37:27', '2023-09-10 21:37:27', NULL, 1, '1694382672.png'),
(58, '1', '2023-09-10 18:42:00', '1', '1', '2023-09-10 21:42:45', '2023-09-10 21:42:45', NULL, 1, '1695148097.jpeg'),
(59, NULL, NULL, NULL, NULL, '2023-09-10 21:47:56', '2023-09-10 21:47:56', NULL, NULL, '1694382672.png'),
(60, '3', '2023-09-10 18:51:00', '3', '3', '2023-09-10 21:52:07', '2023-09-10 21:52:07', NULL, 3, '1695148097.jpeg'),
(61, '1', '2023-09-19 15:27:00', '1', '1', '2023-09-19 18:27:45', '2023-09-19 18:27:45', NULL, 1, '1694382672.png'),
(62, NULL, NULL, NULL, NULL, '2023-09-26 17:08:52', '2023-09-26 17:08:52', NULL, NULL, 'fotos/1695923885.jpeg'),
(63, NULL, NULL, NULL, NULL, '2023-09-26 17:15:11', '2023-09-26 17:15:11', NULL, NULL, 'fotos/1695923885.jpeg'),
(64, NULL, NULL, NULL, NULL, '2023-09-26 17:32:03', '2023-09-26 17:32:03', NULL, NULL, 'fotos/1695923885.jpeg'),
(65, NULL, NULL, NULL, NULL, '2023-09-26 17:51:17', '2023-09-26 17:51:17', NULL, NULL, 'fotos/1695923885.jpeg'),
(66, NULL, NULL, NULL, NULL, '2023-09-26 17:53:02', '2023-09-26 17:53:02', NULL, NULL, 'fotos/1695923885.jpeg'),
(67, 'eventoReserva', '2023-09-27 00:00:00', 'montes claros', 'https://www.speedtest.net/pt', '2023-09-27 22:03:15', '2023-09-27 22:03:15', '1', NULL, 'fotos/1695923885.jpeg'),
(68, 'eventoReserva1', '2023-09-27 00:00:00', 'montesClaros', 'teste1', '2023-09-27 22:04:50', '2023-09-27 22:04:50', '1', NULL, 'fotos/1695923885.jpeg'),
(69, 'eventoReserva2', '2023-09-27 00:00:00', '25', '25', '2023-09-27 22:06:14', '2023-09-27 22:06:14', NULL, NULL, 'fotos/1695923885.jpeg'),
(70, 'teste', '2023-09-28 00:00:00', '1', '1', '2023-09-28 17:53:22', '2023-09-28 17:53:22', NULL, NULL, 'fotos/1695923885.jpeg'),
(71, '1', '2023-09-28 14:57:00', '1', '1', '2023-09-28 17:58:06', '2023-09-28 17:58:06', NULL, 1, 'fotos/1695923885.jpeg'),
(72, '1', NULL, '1', '1', '2023-10-02 12:08:32', '2023-10-02 12:08:32', '1', NULL, NULL),
(73, '2', '2023-10-09 00:00:00', '2', '2', '2023-10-02 12:21:33', '2023-10-02 12:21:33', '1', NULL, NULL),
(74, '3', '2023-10-02 00:00:00', '3', '3', '2023-10-02 12:25:56', '2023-10-02 12:25:56', '1', NULL, NULL),
(75, 'verific', '2023-10-03 00:00:00', 'ver', 'ver', '2023-10-03 16:41:32', '2023-10-03 16:41:32', '1', NULL, NULL),
(76, NULL, NULL, NULL, NULL, '2023-10-03 19:14:52', '2023-10-03 19:14:52', NULL, NULL, ''),
(77, '1', '2023-10-03 00:00:00', '1', '1', '2023-10-03 19:14:52', '2023-10-03 19:14:52', '1', NULL, NULL),
(78, NULL, NULL, NULL, NULL, '2023-10-03 19:17:21', '2023-10-03 19:17:21', NULL, NULL, ''),
(79, '7', '2023-10-03 00:00:00', '7', '7', '2023-10-03 19:17:21', '2023-10-03 19:17:21', '1', NULL, 'not1.jpeg'),
(80, '9', '2023-10-03 00:00:00', '9', '9', '2023-10-03 19:19:16', '2023-10-03 19:19:16', '1', NULL, 'not4.jpg'),
(81, '5', '2023-10-03 00:00:00', '5', '5', '2023-10-03 19:21:30', '2023-10-03 19:21:30', '1', NULL, 'not4.jpg'),
(82, '1', NULL, '1', '1', '2023-10-03 19:27:46', '2023-10-03 19:27:46', '1', NULL, 'not4.jpg'),
(83, '1', '2023-10-03 00:00:00', '1', '1', '2023-10-03 19:40:39', '2023-10-03 19:40:39', '1', NULL, 'fotos/1696362038.jpeg'),
(84, 'teste', NULL, '3', '3', '2023-10-03 21:28:18', '2023-10-03 21:28:18', '3', NULL, NULL),
(85, '1', '2023-10-03 00:00:00', '1', '1', '2023-10-03 21:32:33', '2023-10-03 21:32:33', '1', NULL, NULL),
(86, '1', '2023-10-03 00:00:00', '1', '1', '2023-10-03 21:35:20', '2023-10-03 21:35:20', '1', NULL, NULL),
(87, '1', '2023-10-03 00:00:00', '1', '1', '2023-10-03 21:37:35', '2023-10-03 21:37:35', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `mestrandos`
--

CREATE TABLE `mestrandos` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `monografias`
--

CREATE TABLE `monografias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(250) DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `orientador` varchar(255) DEFAULT NULL,
  `campus` varchar(255) DEFAULT NULL,
  `palavras_chave` text DEFAULT NULL,
  `resumo` text DEFAULT NULL,
  `orientando` varchar(255) DEFAULT NULL,
  `linhasdepesquisa` varchar(100) DEFAULT NULL,
  `anodefesa` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Despejando dados para a tabela `monografias`
--

INSERT INTO `monografias` (`id`, `titulo`, `file`, `created_at`, `updated_at`, `orientador`, `campus`, `palavras_chave`, `resumo`, `orientando`, `linhasdepesquisa`, `anodefesa`) VALUES
(1, '1', '1693224077_Requerimento_metodologia (2)', '2023-08-22 16:48:32', '2023-08-22 16:48:32', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(3, '3', '1693224077_Requerimento_metodologia (2)', '2023-08-22 16:51:23', '2023-08-22 16:51:23', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(4, '4', '1693224077_Requerimento_metodologia (2)', '2023-08-22 17:14:30', '2023-08-22 17:14:30', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(5, '5', '1693224077_Requerimento_metodologia (2)', '2023-08-22 17:15:44', '2023-08-22 17:15:44', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(6, '6', '1693224077_Requerimento_metodologia (2)', '2023-08-22 17:20:11', '2023-08-22 17:20:11', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(7, '7', '1693224077_Requerimento_metodologia (2)', '2023-08-22 17:23:26', '2023-08-22 17:23:26', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(8, '8', '1693224077_Requerimento_metodologia (2)', '2023-08-22 17:28:12', '2023-08-22 17:28:12', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(9, '9', '1693224077_Requerimento_metodologia (2)', '2023-08-22 17:45:05', '2023-08-22 17:45:05', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(10, '10', '1693224077_Requerimento_metodologia (2)', '2023-08-22 18:10:30', '2023-08-22 18:10:30', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(11, '11', '1693224077_Requerimento_metodologia (2)', '2023-08-22 18:10:37', '2023-08-22 18:10:37', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(12, '12', '1693224077_Requerimento_metodologia (2)', '2023-08-23 21:21:40', '2023-08-23 21:21:40', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(13, '13', '1693224077_Requerimento_metodologia (2)', '2023-08-24 19:19:35', '2023-08-24 19:19:35', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(14, '14', '1693224077_Requerimento_metodologia (2)', '2023-08-24 19:19:38', '2023-08-24 19:19:38', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(15, '15', '1693224077_Requerimento_metodologia (2)', '2023-08-28 12:02:33', '2023-08-28 12:02:33', '1', '1', '1', '1', '1', NULL, NULL),
(16, '16', '1693224077_Requerimento_metodologia (2)', '2023-08-28 12:08:59', '2023-08-28 12:08:59', '2', '2', '2', '2', '2', NULL, NULL),
(17, '17', '1693224077_Requerimento_metodologia (2)', '2023-08-28 12:11:44', '2023-08-28 12:11:44', '2', '2', '2', '2', '2', NULL, NULL),
(18, '18', '1693224077_Requerimento_metodologia (2)', '2023-08-28 12:21:22', '2023-08-28 12:21:22', '2', '2', '2', '2', '2', NULL, NULL),
(19, '19', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586571_documento (7).pdf', '2023-09-01 16:42:51', '2023-09-01 16:42:51', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(20, '20', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586574_documento (7).pdf', '2023-09-01 16:42:54', '2023-09-01 16:42:54', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(21, '21', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586574_documento (7).pdf', '2023-09-01 16:42:54', '2023-09-01 16:42:54', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(22, '22', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586574_documento (7).pdf', '2023-09-01 16:42:54', '2023-09-01 16:42:54', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(23, '23', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586575_documento (7).pdf', '2023-09-01 16:42:55', '2023-09-01 16:42:55', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(24, '24', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586578_documento (7).pdf', '2023-09-01 16:42:58', '2023-09-01 16:42:58', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(25, '25', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586579_documento (7).pdf', '2023-09-01 16:42:59', '2023-09-01 16:42:59', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(26, '26', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586579_documento (7).pdf', '2023-09-01 16:42:59', '2023-09-01 16:42:59', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(27, '27', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586579_documento (7).pdf', '2023-09-01 16:42:59', '2023-09-01 16:42:59', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(28, '28', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586580_documento (7).pdf', '2023-09-01 16:43:00', '2023-09-01 16:43:00', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(29, '29', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586580_documento (7).pdf', '2023-09-01 16:43:00', '2023-09-01 16:43:00', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(30, '30', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586580_documento (7).pdf', '2023-09-01 16:43:00', '2023-09-01 16:43:00', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(31, '31', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586580_documento (7).pdf', '2023-09-01 16:43:00', '2023-09-01 16:43:00', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(32, '32', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586581_documento (7).pdf', '2023-09-01 16:43:01', '2023-09-01 16:43:01', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(33, '33', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586657_documento (7).pdf', '2023-09-01 16:44:17', '2023-09-01 16:44:17', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(34, '34', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586659_documento (7).pdf', '2023-09-01 16:44:19', '2023-09-01 16:44:19', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(35, '35', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586659_documento (7).pdf', '2023-09-01 16:44:19', '2023-09-01 16:44:19', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(36, '36', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586659_documento (7).pdf', '2023-09-01 16:44:19', '2023-09-01 16:44:19', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(37, '37', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586660_documento (7).pdf', '2023-09-01 16:44:20', '2023-09-01 16:44:20', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(38, '38', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586660_documento (7).pdf', '2023-09-01 16:44:20', '2023-09-01 16:44:20', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(39, '39', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586660_documento (7).pdf', '2023-09-01 16:44:20', '2023-09-01 16:44:20', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(40, '40', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586660_documento (7).pdf', '2023-09-01 16:44:20', '2023-09-01 16:44:20', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(41, '41', 'http://localhost/Sistema_DMTE-main/backend/public/pdfs/1693586660_documento (7).pdf', '2023-09-01 16:44:20', '2023-09-01 16:44:20', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(42, '42', '', '2023-09-01 17:39:38', '2023-09-01 17:39:38', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(43, '43', '', '2023-09-01 19:28:30', '2023-09-01 19:28:30', 'teste', 'teste', 'teste', 'teste', 'teste', NULL, NULL),
(44, '44', '', '2023-09-01 19:32:11', '2023-09-01 19:32:11', 'teste', 'teste', 'teste', 'teste', 'teste', 'Linguagem', NULL),
(45, '45', '', '2023-09-01 19:33:43', '2023-09-01 19:33:43', 'teste', 'teste', 'teste', 'teste', 'teste', 'Linguagem', '2023-09-01'),
(46, '46', '1693597400_documento (7)', '2023-09-01 19:43:20', '2023-09-01 19:43:20', 'teste', 'teste', 'teste', 'teste', 'teste', 'Educação Histórica', '2023-09-01'),
(47, '47', '1693597466_documento (7)', '2023-09-01 19:44:26', '2023-09-01 19:44:26', 'teste', 'teste', 'teste', 'teste', 'teste', 'Educação Histórica', '2023-09-01'),
(48, '48', '1695840756_000373360.pdf', '2023-09-27 18:52:36', '2023-09-27 18:52:36', '1', '1', '1', '1', '1', 'Linguagem', '2023-09-27'),
(50, 'teste', 'pdfs/1695843302_curriculo_lattes.pdf', '2023-09-27 19:35:02', '2023-09-27 19:35:02', '1', '1', '1', '1', '1', 'Linguagem', '2023-09-27'),
(52, 'oi', 'pdfs/1695854882_000373360.pdf', '2023-09-27 22:48:02', '2023-09-27 22:48:02', 'alo', 'com', 'beb', 'dwjdajwd', 'alos', 'Linguagem', '2023-09-27'),
(53, '1', 'pdfs/1696248539_documento (11).pdf', '2023-10-02 12:08:59', '2023-10-02 12:08:59', '1', '1', '1', '1', '1', 'Linguagem', '2023-10-02'),
(54, '1', 'pdfs/1696248965_documento (10).pdf', '2023-10-02 12:16:05', '2023-10-02 12:16:05', '1', '1', '1', '1', '1', 'Educação Histórica', '2023-10-17'),
(55, '1', 'pdfs/1696249078_documento (10).pdf', '2023-10-02 12:17:58', '2023-10-02 12:17:58', '1', '1', '1', '1', '1', 'Educação Histórica', '2023-10-17'),
(56, '1', 'pdfs/1696249156_modelo.pdf', '2023-10-02 12:19:16', '2023-10-02 12:19:16', '1', '1', '1', '1', '1', 'Educação Histórica', '2023-10-02'),
(57, 'verificação', 'pdfs/1696351231_documento (12).pdf', '2023-10-03 16:40:31', '2023-10-03 16:40:31', 'ver', '1', '1', '1', 'ver', 'Educação Histórica', '2023-10-03');

-- --------------------------------------------------------

--
-- Estrutura para tabela `notasposgraduacao`
--

CREATE TABLE `notasposgraduacao` (
  `id` int(11) NOT NULL,
  `Nome` varchar(100) DEFAULT NULL,
  `Local` varchar(100) DEFAULT NULL,
  `ValorNota` float DEFAULT NULL,
  `fk_professor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `texto` text DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Despejando dados para a tabela `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `texto`, `file`, `created_at`, `updated_at`) VALUES
(1, 'teste', 'o candidato xxxxx com historico xxxxxxxxxxxx xxxxxxxxx xxxxxxxxxxxxx xxxxxxxxxxxxxxxxxxx', 'fotos/1695756450.jpeg', '2023-09-26 16:40:49', '2023-09-26 16:40:49'),
(2, 'testesss', 'o candidato xxxxx com historico xxxxxxxxxxxx xxxxxxxxx xxxxxxxxxxxxx xxxxxxxxxxxxxxxxxxx', 'fotos/1695756450.jpeg', '2023-09-26 16:43:18', '2023-09-26 16:43:18'),
(3, '1', 'o candidato xxxxx com historico xxxxxxxxxxx xxxxxxxxxxxxx xxxxxxxxxxxxxxxx xxxxxxxxxxxxx', 'fotos/1695750886.jpeg', '2023-09-26 17:54:47', '2023-09-26 17:54:47'),
(4, 'fake news', 'o candidato xxxxx com historico xxxxxxxxxxxxxxx xxxxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx', 'fotos/1695756450.jpeg', '2023-09-26 19:27:30', '2023-09-26 19:27:30'),
(27, 'a', 'a', NULL, '2023-10-03 18:26:26', '2023-10-03 18:26:26'),
(28, 'a', 'a', 'not4.jpg', '2023-10-03 18:29:54', '2023-10-03 18:29:54'),
(29, 'b', 'b', 'not4.jpg', '2023-10-03 18:32:07', '2023-10-03 18:32:07'),
(30, 'c', 'c', NULL, '2023-10-03 18:43:27', '2023-10-03 18:43:27'),
(31, 'd', 'd', NULL, '2023-10-03 18:44:32', '2023-10-03 18:44:32'),
(32, 'e', 'e', NULL, '2023-10-03 18:44:49', '2023-10-03 18:44:49'),
(33, 'f', 'f', 'not4.jpg', '2023-10-03 18:47:58', '2023-10-03 18:47:58'),
(34, 'v', 'v', 'not3.jpeg', '2023-10-03 18:49:11', '2023-10-03 18:49:11'),
(35, 't', 't', 'not1.jpeg', '2023-10-03 19:05:11', '2023-10-03 19:05:11'),
(36, 'f', 'f', NULL, '2023-10-03 20:29:35', '2023-10-03 20:29:35'),
(37, 'h', 'h', NULL, '2023-10-03 20:33:19', '2023-10-03 20:33:19'),
(38, 'j', 'j', NULL, '2023-10-03 20:35:47', '2023-10-03 20:35:47'),
(39, 'i', 'i', NULL, '2023-10-03 20:37:21', '2023-10-03 20:37:21'),
(40, 'kk', 'kk', NULL, '2023-10-03 20:39:42', '2023-10-03 20:39:42'),
(41, '!!!!!', 'e', NULL, '2023-10-03 20:45:34', '2023-10-03 20:45:34'),
(42, '!!!!!', '!!!!', NULL, '2023-10-03 20:50:29', '2023-10-03 20:50:29'),
(43, '????', '????', NULL, '2023-10-03 20:56:51', '2023-10-03 20:56:51'),
(44, '!!', '!!', NULL, '2023-10-03 21:20:43', '2023-10-03 21:20:43'),
(45, '1', '1', NULL, '2023-10-03 21:23:16', '2023-10-03 21:23:16'),
(46, '6', '6', NULL, '2023-10-03 21:25:06', '2023-10-03 21:25:06'),
(47, 'p', 'p', NULL, '2023-10-03 21:27:18', '2023-10-03 21:27:18');

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Despejando dados para a tabela `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('00978627d1735b5092adbadf7f76a9450632e4b59c5fd8b962e41b87e2bd19acbf5106d67f541729', 13, 2, NULL, '[]', 0, '2023-09-29 18:02:17', '2023-09-29 18:02:17', '2024-09-29 15:02:17'),
('015d4415b34b4740f79ce68f1e4ffd5ef0efa8356a021288483d1d7cc2fb74ee26cd733e20c85e4e', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:05', '2023-02-15 18:15:05', '2024-02-15 12:15:05'),
('065b57817f537ac16adebadb6ff06f45dcb88b9c0614dce481a721feff9f1e228b6adf3b9c55d46d', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:05', '2023-02-15 18:15:05', '2024-02-15 12:15:05'),
('080936c6810fe28f2491e66165beb0f39fb64f0d62d21b16003546e0897dd26cb788abf6d3ae0341', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:03', '2023-02-15 18:15:03', '2024-02-15 12:15:03'),
('0b395f43cea2ee91e56fbc94633b607e0f5c8c8bdbd66dedf523273d7aaaa102c23ccf1512dbd454', 1, 2, NULL, '[]', 0, '2023-08-20 14:55:49', '2023-08-20 14:55:49', '2024-08-20 11:55:49'),
('0d6f405d1c2d7a6d6a98b325c95af908734db5ca0ef12f05ef2d2736669215c9cfd31f90390c7ba8', 1, 2, NULL, '[]', 0, '2023-06-05 15:31:56', '2023-06-05 15:31:56', '2024-06-05 09:31:56'),
('0e7b814cd7603e04c1edeeaf7d713c87d4a52f27e7514d37a55656f5583f0f4022d79bc8fc6341e1', 1, 2, NULL, '[]', 0, '2023-02-15 18:16:23', '2023-02-15 18:16:23', '2024-02-15 12:16:23'),
('10daceb4cd5a0335406a766e116ebc748206bb87c3a7f4a7e6b934933bf1210815d3a4d0653814b0', 1, 2, NULL, '[]', 0, '2023-02-24 17:44:37', '2023-02-24 17:44:37', '2024-02-24 11:44:37'),
('13de714374a509bb86301a7e87e3075a99ab926f6084e51a850e73bb7ad8e312e403b16b4fb401fe', 13, 2, NULL, '[]', 0, '2023-09-28 20:21:09', '2023-09-28 20:21:09', '2024-09-28 17:21:09'),
('17b372031ad4dc449d7e51eb02c3c8ee40b26488932b82fcaf774b5af57930aac9f3f82429c7fa00', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:04', '2023-02-15 18:15:04', '2024-02-15 12:15:04'),
('186b2f5f5229ba62b39a96b49c38716c7fefb64d7fc6c5a87ff1fdad6a1d7a23dac3a1321a82f810', 1, 2, NULL, '[]', 0, '2023-08-07 15:49:00', '2023-08-07 15:49:00', '2024-08-07 12:49:00'),
('186f280b25e2671c9a29c90d8cc675444588e76af8e0d55c1bf9aa23b41e78fe1d3f8d091a8a4444', 1, 2, NULL, '[]', 0, '2023-08-15 12:01:58', '2023-08-15 12:01:58', '2024-08-15 09:01:58'),
('1c2f8754cbc8d91d574f20de4887cf342df3514f60c0de1384b52ea62e3660b901f02d7acc1e5a0b', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:04', '2023-02-15 18:15:04', '2024-02-15 12:15:04'),
('1c3101712b9cdeb35e5bac418c6a6edde96fd0f9ee7b7b4654c5468063f03614dddf6c557b66194d', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:04', '2023-02-15 18:15:04', '2024-02-15 12:15:04'),
('1c71f19fc9491d4ff53af741bfef1eb57d009fff9f707ce7d7f1e11cc9003d4f4438dc566e6db703', 1, 2, NULL, '[]', 0, '2023-05-19 00:59:16', '2023-05-19 00:59:16', '2024-05-18 18:59:16'),
('1cd5f87394913523e799147590bc6e229c8b6a04b960609fc829df2887a9442d66da6a909fe0d040', 1, 2, NULL, '[]', 0, '2023-08-24 19:13:41', '2023-08-24 19:13:41', '2024-08-24 16:13:41'),
('29469e4ed44154c84abef7d68cbcf369bec7e833c3b715daf8e9ddd7bc8b84c3630bc180233bff11', 1, 2, NULL, '[]', 0, '2023-08-05 13:59:35', '2023-08-05 13:59:35', '2024-08-05 10:59:35'),
('3310541ef8d65e064f4a494aad51431d3eee07f7a0d72703831654784c570163047b64a05d4bd94a', 1, 2, NULL, '[]', 0, '2023-08-10 19:20:25', '2023-08-10 19:20:25', '2024-08-10 16:20:25'),
('33c40f1333582e641604d18f0eee13ab068cd46bae1f69a4ee7277e5f7a5f28720bfe978472a4236', 1, 2, NULL, '[]', 0, '2023-02-14 16:57:13', '2023-02-14 16:57:13', '2024-02-14 10:57:13'),
('3835f88f5f9fc8c2f47ae44d3de387a1198389fc957f87f709530ffe855405b01a18aa3bc863c37e', 1, 2, NULL, '[]', 0, '2023-10-02 16:57:14', '2023-10-02 16:57:14', '2024-10-02 13:57:14'),
('3fa6f34e592937d0143e1ab5e891a24383530cfb5532244ff22e4cdbea9b84c582934fcbf7ac6029', 1, 2, NULL, '[]', 0, '2023-06-05 01:46:59', '2023-06-05 01:46:59', '2024-06-04 19:46:59'),
('3fb01daa9ce886229909802bca45e60b403e76d94946efff5db2475c175b66bd6bab14bacc696850', 1, 2, NULL, '[]', 0, '2023-08-22 13:04:02', '2023-08-22 13:04:02', '2024-08-22 10:04:02'),
('40c2c3c5ca6d2d497407a69f5533020d6bee141409ab361112c3428c8965f3038b3e70f29b9d8b5a', 1, 2, NULL, '[]', 0, '2023-08-09 14:54:59', '2023-08-09 14:54:59', '2024-08-09 11:54:59'),
('41b0c48c4e9ac4adf36c397de1c33830fd9637e075226d3e51e680b3783ccc60aac995c74a614a74', 1, 2, NULL, '[]', 0, '2023-06-01 01:26:12', '2023-06-01 01:26:12', '2024-05-31 19:26:12'),
('4236f9280f873cd772cda3c0cc603a5ecf70522abd92b685dc8c3b9ff1fb783decc8547ecd6d38e4', 1, 2, NULL, '[]', 0, '2023-08-07 20:33:35', '2023-08-07 20:33:35', '2024-08-07 17:33:35'),
('42583f9bd7f43fa7b2f46193dce731da10333dcfd2ba9db4fbe62b3b8799bf19837c99268030a319', 1, 2, NULL, '[]', 0, '2023-10-02 17:27:34', '2023-10-02 17:27:34', '2024-10-02 14:27:34'),
('4273d412f5d9ef401dbd2686531e373dd58de808b83c69da6db23db623ecba0894c0e3a6e81a3e6e', 1, 2, NULL, '[]', 0, '2023-06-04 23:58:32', '2023-06-04 23:58:32', '2024-06-04 17:58:32'),
('43c99e9688e1c68e7012d18b4625ef0452021a2a78340c3be9f1fe76f02409d5cb3f24d31671f6b9', 1, 2, NULL, '[]', 0, '2023-06-05 02:52:29', '2023-06-05 02:52:29', '2024-06-04 20:52:29'),
('4ab2dc67a42c9742bf30997b5c6f1a20bec953de6a380454a312448488282df680b00eb46d4ebdb9', 1, 2, NULL, '[]', 0, '2023-08-20 14:53:44', '2023-08-20 14:53:44', '2024-08-20 11:53:44'),
('4e504b019c52b5a3d5dcd8783889e62ca54fa74475ea1e0c611a2f7f26cd2c3ea3d3be60cdc461ea', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:03', '2023-02-15 18:15:03', '2024-02-15 12:15:03'),
('4fd8050281ea7f37003e5b128b30bf98341b0d6d6c9040b112aa6f19922b3a937b15468d662fe12b', 1, 2, NULL, '[]', 0, '2023-08-08 16:46:52', '2023-08-08 16:46:52', '2024-08-08 13:46:52'),
('503838b9891181a57a1b2bf2b4e582340f1d3cae30fd7107ae8df5c3eaa9400bab65a00a7f77c1b8', 1, 2, NULL, '[]', 0, '2023-08-05 13:59:32', '2023-08-05 13:59:32', '2024-08-05 10:59:32'),
('557cb08b5a4a5b45cbb17f95a51d8b4c8b37a421fdda22b00984de4c3a4b5e601437ad311e09ce13', 1, 2, NULL, '[]', 0, '2023-08-28 12:26:37', '2023-08-28 12:26:37', '2024-08-28 09:26:37'),
('5af044d19f8d49ccb0bbad4d498ed7f9ffa7cf1ac2487e7db8de6277bf4a5d5a951f8919c07e9564', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:02', '2023-02-15 18:15:02', '2024-02-15 12:15:02'),
('5b8b33a07bafcae6c1bc7fd647be75cb9e959a31f606df8aaad7fb11c7c3429e745fd952b5497f99', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:05', '2023-02-15 18:15:05', '2024-02-15 12:15:05'),
('5b9507bc0f5efeee50f567983a5c009dd70accf6a1b267b64a2e1c00658ff00f32b43713d17800cd', 1, 2, NULL, '[]', 0, '2023-06-02 01:41:16', '2023-06-02 01:41:16', '2024-06-01 19:41:16'),
('5c23557817cb9504a68af783b6a6d9daf2fdb36010065521b106b8e0ad379147146c4cf6116c9760', 1, 2, NULL, '[]', 0, '2023-06-02 00:40:26', '2023-06-02 00:40:26', '2024-06-01 18:40:26'),
('5f21d54b08f08ff3ea75d25baa4759572d9c1e9bcdfd0849285e5c1a3cddf783758cd7c9294ed408', 1, 2, NULL, '[]', 0, '2023-08-08 12:00:09', '2023-08-08 12:00:09', '2024-08-08 09:00:09'),
('664e1f238e7eafc698ad4c08d2c5cbc660a2756bd2fb3945fc8566be72dd325cb6caf7ed43fc6c71', 1, 2, NULL, '[]', 0, '2023-02-16 20:25:31', '2023-02-16 20:25:31', '2024-02-16 14:25:31'),
('67679f23025c579730037e1d467822192657697b741d5b19e2e4c58b62a5eaf2c4f9028e5322c390', 1, 2, NULL, '[]', 0, '2023-06-05 01:55:13', '2023-06-05 01:55:13', '2024-06-04 19:55:13'),
('6e1fd6984893a39ac1256e5241f662c95977d49f92f7016cbe2f69f566bf3ff43373f5537ec9ccaf', 1, 2, NULL, '[]', 0, '2023-08-13 18:51:40', '2023-08-13 18:51:40', '2024-08-13 15:51:40'),
('721949268dbbfaaacf0e89965d4291c60fa37759b106f470af3183917f75b93122fe7b51ae0971e0', 1, 2, NULL, '[]', 0, '2023-06-03 22:32:22', '2023-06-03 22:32:22', '2024-06-03 16:32:22'),
('789a9ba2ea0b6ece7bfb29a00841c200d063e019dd16d757312a00da4a3f5537b93e997b14d4161b', 1, 2, NULL, '[]', 0, '2023-08-05 14:37:28', '2023-08-05 14:37:28', '2024-08-05 11:37:28'),
('7af6a86b6e5ebb9399eeec8641a629e561f00abc8c4d4536520a797550c6c6e566c8be99be529f16', 1, 2, NULL, '[]', 0, '2023-06-03 23:08:29', '2023-06-03 23:08:29', '2024-06-03 17:08:29'),
('7b4ac85f8a76c2904f9338b44429fee5556e4c1b892ceef18f233a850d29d3503de8edd84cc08818', 1, 2, NULL, '[]', 0, '2023-02-14 17:10:24', '2023-02-14 17:10:24', '2024-02-14 11:10:24'),
('7dbc83d9fb8dd7a97b8664be9f5993a887fb97712de8ddb29f7116da8c73285dc30db8ed160c8e26', 13, 2, NULL, '[]', 0, '2023-10-02 14:12:10', '2023-10-02 14:12:10', '2024-10-02 11:12:10'),
('7e98469845a31642952f2a84f0847f3013d42079dcb3e9056a99f727793634137c824916d73c3ef6', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:04', '2023-02-15 18:15:04', '2024-02-15 12:15:04'),
('84e9651c739a35d93d4a901428762fb552299294def88d29252c95a2123363debc738e6cbb84ff10', 1, 2, NULL, '[]', 0, '2023-04-20 23:38:18', '2023-04-20 23:38:18', '2024-04-20 17:38:18'),
('85457fac9330405203949f6371873ee4722a15ad5e835363ab2bc34c334db93f37c0c97541564d55', 1, 2, NULL, '[]', 0, '2023-08-06 14:16:38', '2023-08-06 14:16:38', '2024-08-06 11:16:38'),
('85e6b52ec93a951cec331337cfd8e12447fab68c661a623fd1c0d4868e36ab68bdeabe8aece9c05e', 1, 2, NULL, '[]', 0, '2023-02-16 16:52:44', '2023-02-16 16:52:44', '2024-02-16 10:52:44'),
('878033415e9414df6ebf50e54a08df13e8b12e59c51717d4f0189f0326e0f3946adb1a20829fca2b', 1, 2, NULL, '[]', 0, '2023-09-28 19:59:36', '2023-09-28 19:59:36', '2024-09-28 16:59:36'),
('8a94e2df1293dc9e7284933c198540f8192c66f342d9d67c54c2f1603bc202e87cd822afbc92d674', 1, 2, NULL, '[]', 0, '2023-02-15 18:14:55', '2023-02-15 18:14:55', '2024-02-15 12:14:55'),
('8cf9831efe57f6a8f681941714666063235e332f35a74a30d461656e321cbadabbd404ed711a13c3', 1, 2, NULL, '[]', 0, '2023-04-20 23:38:07', '2023-04-20 23:38:07', '2024-04-20 17:38:07'),
('8f803c70354ed38abb5b2a482a218e28ef06050e2d42e710a3af0e54fa87a4d4d851de9258a44360', 1, 2, NULL, '[]', 0, '2023-06-02 17:11:24', '2023-06-02 17:11:24', '2024-06-02 11:11:24'),
('912c199bffd5c1c6d7c7984a0c68f5365fb4d67243ab6a387138190a52090caef2bf054d61a87622', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:04', '2023-02-15 18:15:04', '2024-02-15 12:15:04'),
('91573197ca686ceac0ae5ba68b080144597c0aa2c47daf4ec9daf0749922a77e2938b849dd70b998', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:04', '2023-02-15 18:15:04', '2024-02-15 12:15:04'),
('919bec810409f04ffea8afde0aa7e25d35a9095b7df2813359ae2c2f9ca0e004ae5f201523528dae', 1, 2, NULL, '[]', 0, '2023-06-01 01:12:46', '2023-06-01 01:12:46', '2024-05-31 19:12:46'),
('93144045ea39f1248612c7b38387359e4f6cbdd956444a454d07eb8a736f91a9e4fed8001133db75', 1, 2, NULL, '[]', 0, '2023-02-23 20:51:09', '2023-02-23 20:51:09', '2024-02-23 14:51:09'),
('95213c14d496c549e1f45996ecbf1dbf990d4042c2faad1c67e42711227a7b86767dc5804ca26809', 1, 2, NULL, '[]', 0, '2023-09-27 16:57:17', '2023-09-27 16:57:17', '2024-09-27 13:57:17'),
('99627a3a6c66f62e4ab9955f6afb70ff9f7306087ba9b8a84ef10197a3e4adf26f95a6a31e97ae01', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:04', '2023-02-15 18:15:04', '2024-02-15 12:15:04'),
('9ab0687e314fab8723e78168dc28b3bb1a849e286f0b09339ba34db49dc9c3ab9566ad019f15e001', 1, 2, NULL, '[]', 0, '2023-02-15 18:14:57', '2023-02-15 18:14:57', '2024-02-15 12:14:57'),
('a34f008429a28b4a21610d1c8cfefbdd75f81d06f3cd1ce1856dea5a007fda78abd1f7ed63161f1a', 1, 2, NULL, '[]', 0, '2023-02-15 18:17:33', '2023-02-15 18:17:33', '2024-02-15 12:17:33'),
('a47a5ea532702bdae63ecc066251ecaa53838519f010b0dd41d1993921b4695d242c238c56ea2c81', 1, 2, NULL, '[]', 0, '2023-06-03 23:05:12', '2023-06-03 23:05:12', '2024-06-03 17:05:12'),
('aa4b9ba6f605728c4f3bdbf90278b6bfc1940d02c58016cf09c5c9b1a8f98961ccec75b4097c127c', 1, 2, NULL, '[]', 0, '2023-09-09 14:19:36', '2023-09-09 14:19:36', '2024-09-09 11:19:36'),
('ab05f37509c8281bdf73b08bd537b75af1e5b45b985a8b9eb8944457554ab86beebe0af86c8c51c9', 1, 2, NULL, '[]', 0, '2023-08-07 15:49:18', '2023-08-07 15:49:18', '2024-08-07 12:49:18'),
('acfdcf550be8e25e92c0951adbbdeb928e141157608df7c6bccad1d73d8f08b530c3ccc507b5ce15', 1, 2, NULL, '[]', 0, '2023-08-05 14:26:57', '2023-08-05 14:26:57', '2024-08-05 11:26:57'),
('ad6f9619d85c6ba361e7eadd601e09e358cb5a4a65992edac0a38d4f3648dae00c834a9c948090f0', 1, 2, NULL, '[]', 0, '2023-08-22 13:04:05', '2023-08-22 13:04:05', '2024-08-22 10:04:05'),
('afd01a2ecc5a240ae57cf957539ee3eab5d01dd606c28f2b738494bb886aa8f022c424a51e5a2934', 1, 2, NULL, '[]', 0, '2023-08-16 21:30:19', '2023-08-16 21:30:19', '2024-08-16 18:30:19'),
('b2f82396359a6f4890a2ca1d53e07c3897574225c90c4d18f66a47159e734f19cf39236f5fb04353', 1, 2, NULL, '[]', 0, '2023-06-04 23:55:46', '2023-06-04 23:55:46', '2024-06-04 17:55:46'),
('b54cc09aaa4b61b798e2e91dca690152b2110c4d1216ba5755a95363968a69800972f6649210b22b', 1, 2, NULL, '[]', 0, '2023-06-05 04:07:18', '2023-06-05 04:07:18', '2024-06-04 22:07:18'),
('b7340f82d6f60aff390b099b850adc921c5bf120ec7a196b345978c42d481535d23ee87ea799d15e', 1, 2, NULL, '[]', 0, '2023-08-06 13:52:36', '2023-08-06 13:52:36', '2024-08-06 10:52:36'),
('b8c7571aba4d1abfd60d2277f3ba85a5917d6d57385c230ef578dea3e6c9699fc63b9244dfce3771', 1, 2, NULL, '[]', 0, '2023-02-16 16:52:43', '2023-02-16 16:52:43', '2024-02-16 10:52:43'),
('bb4585767c6fa92410e4c1516b3b74facd0e67928a6e14380b019bed3175099a41746819f4083d31', 1, 2, NULL, '[]', 0, '2023-09-28 20:00:02', '2023-09-28 20:00:02', '2024-09-28 17:00:02'),
('bdb104f527d468189d494a8cdb4fdfdd3175f53b6fb405c1bb702770137a015ada1e562dcab12568', 1, 2, NULL, '[]', 0, '2023-08-10 19:06:18', '2023-08-10 19:06:18', '2024-08-10 16:06:18'),
('c4626bc68fecfdf75c18cb0987ee5550e2f304d587d3636ba4e770e59e401be11201dff28a50d7e8', 1, 2, NULL, '[]', 0, '2023-10-02 17:29:33', '2023-10-02 17:29:33', '2024-10-02 14:29:33'),
('c58704af45f34036bce972020309eb10062067b1a95a879e8cb10ee34ea18a07aef10c38876baae4', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:03', '2023-02-15 18:15:03', '2024-02-15 12:15:03'),
('c709f39aec2935e507357e2d8f59b0b262cb7f9b30db03428fe58248507e1b776be1323d2a0ea3a1', 1, 2, NULL, '[]', 0, '2023-06-05 14:29:33', '2023-06-05 14:29:33', '2024-06-05 08:29:33'),
('c80715d06c9c7be367893ee25a52faccd0e838d8d6705cef13ae301bfdf56f0d8c47a8b628ecc8a6', 5, 2, NULL, '[]', 0, '2023-08-24 19:12:34', '2023-08-24 19:12:34', '2024-08-24 16:12:34'),
('c9c2e016bf1cd7cf4cc478c4ec3915956d3163b7f4115a4d599023bbcf21a3ed1e9b44642546c5c0', 13, 2, NULL, '[]', 0, '2023-10-02 17:27:47', '2023-10-02 17:27:47', '2024-10-02 14:27:47'),
('cca0b4fee59c12b85880d5216c997fb679bf1166a9d3f66f202a36e85126c26ed179804c0281ad5a', 1, 2, NULL, '[]', 0, '2023-04-20 23:39:29', '2023-04-20 23:39:29', '2024-04-20 17:39:29'),
('cf0e009987641a83ef494e4e6d0659c8c1f12b547491da479b3874f372e4a140e6c1c09ee9fc94b1', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:05', '2023-02-15 18:15:05', '2024-02-15 12:15:05'),
('cf9d66976e143ca800f641a2eb2b75eb16d85f00587cf3c199df6c0ccf389892cc6e2beb372abe77', 13, 2, NULL, '[]', 0, '2023-09-29 18:04:05', '2023-09-29 18:04:05', '2024-09-29 15:04:05'),
('d5998595ef08efd07d75a6bc45a5527fb1affde20257f2ca333c2550e5258f423bed8ece4f0fb029', 1, 2, NULL, '[]', 0, '2023-10-02 12:10:54', '2023-10-02 12:10:54', '2024-10-02 09:10:54'),
('d8fbb3ebd44d661a29f5f541a66acf2029f6279853428729226e4a0df21d04cec1aefbb10f938125', 1, 2, NULL, '[]', 0, '2023-08-22 13:04:04', '2023-08-22 13:04:04', '2024-08-22 10:04:04'),
('db677948bfdc57393116edd9c82002c99dbfb997e85d360a710ccb4e78101b441c6828059ba81a8f', 1, 2, NULL, '[]', 0, '2023-09-27 20:25:14', '2023-09-27 20:25:14', '2024-09-27 17:25:14'),
('dbc60640d04632511118f7edad4c24cea8cb1d68bafc5e6156420cbf60e76b55f819c13c695f379f', 1, 2, NULL, '[]', 0, '2023-06-04 19:21:39', '2023-06-04 19:21:39', '2024-06-04 13:21:39'),
('e814ac44826069bd2c3e48a629881023970b5fea9ff952c9be86ef4b2e325790d3260343438059b4', 1, 2, NULL, '[]', 0, '2023-10-03 17:02:21', '2023-10-03 17:02:21', '2024-10-03 14:02:21'),
('e95009ad3f63c3c24bb49aad9c1f0d426d37b0facb71188dde8159693ffc7d3f0f79275bd9782271', 1, 2, NULL, '[]', 0, '2023-08-31 13:26:10', '2023-08-31 13:26:10', '2024-08-31 10:26:10'),
('ecd3864c4ec8838a5a36ab9f5294bb1f70fe9af39c1ae062b50cec2fdf51d3bc0bc7b19da786061e', 1, 2, NULL, '[]', 0, '2023-09-27 21:40:19', '2023-09-27 21:40:19', '2024-09-27 18:40:19'),
('ee60ccb7e35eb8009e3c026719b2a93248bfec8eec746c036a988bb7db53ecbf09909ef6b4778713', 13, 2, NULL, '[]', 0, '2023-09-28 20:20:07', '2023-09-28 20:20:07', '2024-09-28 17:20:07'),
('f3d2e590025725ffd1af3b0f443be518d4f036dd14e38a48abd3004f67ec1ec5f054cb8e20989a8b', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:05', '2023-02-15 18:15:05', '2024-02-15 12:15:05'),
('f5e0c014db2faef4b87774034833b47161cf4265e274455062648b61492e866c89c202d0a37ccdec', 1, 2, NULL, '[]', 0, '2023-02-15 18:15:05', '2023-02-15 18:15:05', '2024-02-15 12:15:05'),
('f74732b27b964d86555b3ba3489346d7d58a1d0e7c2be81825033ca6579c4397abcc12f6f7ec9826', 1, 2, NULL, '[]', 0, '2023-02-15 18:14:57', '2023-02-15 18:14:57', '2024-02-15 12:14:57'),
('f7852a1e255314f7a2d80052d5e31280fe460ce3462846b19db535924f09f57752b71b1857a85db3', 1, 2, NULL, '[]', 0, '2023-06-02 00:40:27', '2023-06-02 00:40:27', '2024-06-01 18:40:27'),
('fa07ec45bb149f7ddb6829cf418099ee16e58b2fea8066094b1f54410820dd35c16758b7da61f16b', 1, 2, NULL, '[]', 0, '2023-08-31 01:14:32', '2023-08-31 01:14:32', '2024-08-30 22:14:32');

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Despejando dados para a tabela `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'BweSsAyKprDOWI0jCBTMh3lmLKJSGOxWpTMXS1CR', NULL, 'http://localhost', 1, 0, 0, '2023-01-04 01:53:09', '2023-01-04 01:53:09'),
(2, NULL, 'Laravel Password Grant Client', 'MwRag6W0qrQ5g82YTQJ8ZzgFddIOKSKQnMB22f9n', 'users', 'http://localhost', 0, 1, 0, '2023-01-04 01:53:09', '2023-01-04 01:53:09');

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Despejando dados para a tabela `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2023-01-04 01:53:09', '2023-01-04 01:53:09');

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Despejando dados para a tabela `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('0526fb6a2c7a96146ec486a1d2a3809ea790ec62962b94184aa912d0df64ea544700fb2c565c2399', '789a9ba2ea0b6ece7bfb29a00841c200d063e019dd16d757312a00da4a3f5537b93e997b14d4161b', 0, '2024-08-05 11:37:28'),
('09df966f0e7121ecb87346d56f12131b364527a53d8b693536d757d7778ab783ce5d8e275c7a236d', '9ab0687e314fab8723e78168dc28b3bb1a849e286f0b09339ba34db49dc9c3ab9566ad019f15e001', 0, '2024-02-15 12:14:57'),
('0b28ea7d18c6acf05e86dfc529dab545676033e888edcf3001de1218f2078970823ba345b8b1a719', '00978627d1735b5092adbadf7f76a9450632e4b59c5fd8b962e41b87e2bd19acbf5106d67f541729', 0, '2024-09-29 15:02:18'),
('0c9b9d066da783084729b7d9a134513c160b6ea055f8b9c41a43314ce3b4ee5b53349ad3e14d1c7a', '3835f88f5f9fc8c2f47ae44d3de387a1198389fc957f87f709530ffe855405b01a18aa3bc863c37e', 0, '2024-10-02 13:57:14'),
('0d96395e2f503ab596e74e2457c9a47400859b87d288025405384af2750e7d0434b60049f4908ebd', '6e1fd6984893a39ac1256e5241f662c95977d49f92f7016cbe2f69f566bf3ff43373f5537ec9ccaf', 0, '2024-08-13 15:51:40'),
('103aa53a5dd825f351ae4ae0e0f799674a0b9de9f586be9cf3cfbcff6fb27ccd40f4e2a6b9aa9bbd', '7e98469845a31642952f2a84f0847f3013d42079dcb3e9056a99f727793634137c824916d73c3ef6', 0, '2024-02-15 12:15:04'),
('1449f6a593296dee26a3a4fa9b10b9573e346e50e01eac55a887f749722d77a9dcbde27a0e20d694', '33c40f1333582e641604d18f0eee13ab068cd46bae1f69a4ee7277e5f7a5f28720bfe978472a4236', 0, '2024-02-14 10:57:13'),
('14e24e94e3d6bb804812a0a772269085a37c89c9f64fea0c44a5bcc2571b2597f4187ab3526724fa', 'd8fbb3ebd44d661a29f5f541a66acf2029f6279853428729226e4a0df21d04cec1aefbb10f938125', 0, '2024-08-22 10:04:04'),
('15b28ddaa412a3c6029e400cb83601319433e1fc1589848c48b5e4e2b9ec8d8e775dfa5fe62cae9c', '0e7b814cd7603e04c1edeeaf7d713c87d4a52f27e7514d37a55656f5583f0f4022d79bc8fc6341e1', 0, '2024-02-15 12:16:23'),
('18ff7b84c8dd742d0c09dfe92f453540605e1b703584173fa6eaa585edce3c926122e682b955827f', 'f5e0c014db2faef4b87774034833b47161cf4265e274455062648b61492e866c89c202d0a37ccdec', 0, '2024-02-15 12:15:05'),
('198c2679fec2ea814b4071943b1b9a89747f1639878af96d8581b883efd70ad8ab29fcb972bc9eaf', '7b4ac85f8a76c2904f9338b44429fee5556e4c1b892ceef18f233a850d29d3503de8edd84cc08818', 0, '2024-02-14 11:10:24'),
('20bc3b90f4521110762115d0778e97078c6440cddbd4000513e6db88cbb416f9ad949341a21014c1', '4236f9280f873cd772cda3c0cc603a5ecf70522abd92b685dc8c3b9ff1fb783decc8547ecd6d38e4', 0, '2024-08-07 17:33:35'),
('276e2961e406e8e6c723507c7bc4622f353b7c6dc6ad15f425c8e95b7d996d5794d7791715652e64', '0d6f405d1c2d7a6d6a98b325c95af908734db5ca0ef12f05ef2d2736669215c9cfd31f90390c7ba8', 0, '2024-06-05 09:31:56'),
('2c272ac326618085ed461f71f97bb98448ac179243a0078c42124f8c84519222921cb929d9578761', '0b395f43cea2ee91e56fbc94633b607e0f5c8c8bdbd66dedf523273d7aaaa102c23ccf1512dbd454', 0, '2024-08-20 11:55:49'),
('4733e24c2c215d4f9a83f1ede053095167a2e44d49811a71d1bd88a9c0f2a82c6aff35bdc3b27898', '503838b9891181a57a1b2bf2b4e582340f1d3cae30fd7107ae8df5c3eaa9400bab65a00a7f77c1b8', 0, '2024-08-05 10:59:32'),
('48c00085f52ada58f0252894762691091bc18799c3f0fa17de34189bd65ec18da666e67890941c54', 'fa07ec45bb149f7ddb6829cf418099ee16e58b2fea8066094b1f54410820dd35c16758b7da61f16b', 0, '2024-08-30 22:14:32'),
('49ceaf491dbb7c0800c28e55220ac190b42c8e3d9a03dd4e53be8a5b6f83cfd9d263f703a043f93d', '1c2f8754cbc8d91d574f20de4887cf342df3514f60c0de1384b52ea62e3660b901f02d7acc1e5a0b', 0, '2024-02-15 12:15:04'),
('4baaa50cf76b5e2aaaf2dc1c3ea920b3a0d87a3af99ee0710c75b744640eaf4bb2a7a1d10aed0a21', '40c2c3c5ca6d2d497407a69f5533020d6bee141409ab361112c3428c8965f3038b3e70f29b9d8b5a', 0, '2024-08-09 11:54:59'),
('4dce0faea6b38169f422fc57f53aa2613b40d4a216a9504a3fe8bfad416c3df51c1c0a8af903bebb', 'f7852a1e255314f7a2d80052d5e31280fe460ce3462846b19db535924f09f57752b71b1857a85db3', 0, '2024-06-01 18:40:27'),
('4de4c401e0226ce04c39908fbfd65ce0b2ac7359fc149b35f83bd9daaa4493d5dcd0042e09aae2c1', 'b2f82396359a6f4890a2ca1d53e07c3897574225c90c4d18f66a47159e734f19cf39236f5fb04353', 0, '2024-06-04 17:55:46'),
('4f17b8f7b5a2948e6e5004ea428ddcb3fa8456593f577244c1a9212c2d7004e34066858c48122acf', '065b57817f537ac16adebadb6ff06f45dcb88b9c0614dce481a721feff9f1e228b6adf3b9c55d46d', 0, '2024-02-15 12:15:05'),
('51ed4c022ace927e15e7b2a70d71813869102e76b5fccf1b5e7f1351d60e59934abf98ad30ea4e33', '5f21d54b08f08ff3ea75d25baa4759572d9c1e9bcdfd0849285e5c1a3cddf783758cd7c9294ed408', 0, '2024-08-08 09:00:09'),
('54ecedf51b5f675b05b4171c5520356357d6156b22fc805fc87df646a3c34a79b148522f8d7f4fa9', '42583f9bd7f43fa7b2f46193dce731da10333dcfd2ba9db4fbe62b3b8799bf19837c99268030a319', 0, '2024-10-02 14:27:34'),
('5d506ac29e543d14300619cddcef3c19c45bce89a437fbd519b6a27bc734e51bbd8a855716c60917', '17b372031ad4dc449d7e51eb02c3c8ee40b26488932b82fcaf774b5af57930aac9f3f82429c7fa00', 0, '2024-02-15 12:15:04'),
('61694d4703f8876d6984009c438b75d31627b10a93d2bca75b60370bf71ed5f3a1203fe98e353090', 'b7340f82d6f60aff390b099b850adc921c5bf120ec7a196b345978c42d481535d23ee87ea799d15e', 0, '2024-08-06 10:52:36'),
('61a8f1bb236309994d36e7811f1f6bcc55df50426d9c2a503214a721f2e80144b230796fef9bb533', 'acfdcf550be8e25e92c0951adbbdeb928e141157608df7c6bccad1d73d8f08b530c3ccc507b5ce15', 0, '2024-08-05 11:26:57'),
('64f08a240653159f2f7242169a4ec25b6dd60fa5ca864fa53ea0d5dc9349692595251c5d29649329', '67679f23025c579730037e1d467822192657697b741d5b19e2e4c58b62a5eaf2c4f9028e5322c390', 0, '2024-06-04 19:55:13'),
('656dd8af59d31333886f32b300241d30427b800849f1157bfcd41016e35bb1e6ed672441c457abe1', 'f3d2e590025725ffd1af3b0f443be518d4f036dd14e38a48abd3004f67ec1ec5f054cb8e20989a8b', 0, '2024-02-15 12:15:05'),
('662bf99f4e4ef6a04facdd2eb537c313e4881c7f817ad99116ad3900cad0eae608376860d5620920', '10daceb4cd5a0335406a766e116ebc748206bb87c3a7f4a7e6b934933bf1210815d3a4d0653814b0', 0, '2024-02-24 11:44:37'),
('6acb6ecf397494ba3e2d908f86ae12c7b07bdf4b0c195a05e776cf6fd02db9293fa7a8700027b288', '919bec810409f04ffea8afde0aa7e25d35a9095b7df2813359ae2c2f9ca0e004ae5f201523528dae', 0, '2024-05-31 19:12:46'),
('6c3948b01c3bf4166bcfce8fb196be6cf986ff6c01b43b155a59a859075a79fd776baa655e8576be', '878033415e9414df6ebf50e54a08df13e8b12e59c51717d4f0189f0326e0f3946adb1a20829fca2b', 0, '2024-09-28 16:59:36'),
('6ccf48aea5e70ac000e52637dd012bc8af3079b95aa2de2bdbf913fcdc0b811638517ca20befe101', '4273d412f5d9ef401dbd2686531e373dd58de808b83c69da6db23db623ecba0894c0e3a6e81a3e6e', 0, '2024-06-04 17:58:32'),
('6d1bc7d4d9b2c8da681c3c25e6104b25442d23b537d5689923f88be1ff45c7b6d14cfb0cabf76c42', '186f280b25e2671c9a29c90d8cc675444588e76af8e0d55c1bf9aa23b41e78fe1d3f8d091a8a4444', 0, '2024-08-15 09:01:58'),
('741d27dfacfb7fd8c73a3d502f63b92e3bf84f1c05648f5eb7025b7b1a74f52e557cfd2d101513f7', '41b0c48c4e9ac4adf36c397de1c33830fd9637e075226d3e51e680b3783ccc60aac995c74a614a74', 0, '2024-05-31 19:26:12'),
('76372f8a1d42884ad4b8fc6159c00e14ea174dd7732897444392a6fce642181f1e208d7bfa1f49b1', 'ecd3864c4ec8838a5a36ab9f5294bb1f70fe9af39c1ae062b50cec2fdf51d3bc0bc7b19da786061e', 0, '2024-09-27 18:40:19'),
('7941b290ccb0ff87af7c7e17caf1d31c8780eb633ad5370508a5a68ee383b0eeb5702d5021b6987e', 'c58704af45f34036bce972020309eb10062067b1a95a879e8cb10ee34ea18a07aef10c38876baae4', 0, '2024-02-15 12:15:03'),
('7b3c2a83ff150c408cacc1d79522eaf4a72f7b5f80253a66fa2bcb51f6f51085fabb555578e6081e', '1c71f19fc9491d4ff53af741bfef1eb57d009fff9f707ce7d7f1e11cc9003d4f4438dc566e6db703', 0, '2024-05-18 18:59:16'),
('7c8ec1c7f073fa18b81376e6c5ea9133350fb3eed5133d34c24d226661a3a65096d927921535e2bb', 'f74732b27b964d86555b3ba3489346d7d58a1d0e7c2be81825033ca6579c4397abcc12f6f7ec9826', 0, '2024-02-15 12:14:57'),
('82f8eef2c713cecb67a187569bfd493c78ec07676022bf3e9d6372d1361fd7ec6862a696742a4d58', '85457fac9330405203949f6371873ee4722a15ad5e835363ab2bc34c334db93f37c0c97541564d55', 0, '2024-08-06 11:16:38'),
('8dcdc51ccecb246272a78490213e0c119ad48e3e75b819a50e16a269a4b4e1cbf7257352a8e6756a', 'ee60ccb7e35eb8009e3c026719b2a93248bfec8eec746c036a988bb7db53ecbf09909ef6b4778713', 0, '2024-09-28 17:20:07'),
('8fe27487b15f4fc77c7cbb9037258e2b7b396aa6b52636a3192b84582b8245187a66031ffbe606aa', '7af6a86b6e5ebb9399eeec8641a629e561f00abc8c4d4536520a797550c6c6e566c8be99be529f16', 0, '2024-06-03 17:08:29'),
('8fe8e756b69feb57cfe4b5471c8657afdb4b46f4461d87c858e4aeb35b1ece4c8742edbdbad781d0', 'bb4585767c6fa92410e4c1516b3b74facd0e67928a6e14380b019bed3175099a41746819f4083d31', 0, '2024-09-28 17:00:02'),
('95890de64e86ae2850e1739d31c2749e7abb1d279d1e895e9fae019c2a0012c2bb5ec3d072fba9db', 'aa4b9ba6f605728c4f3bdbf90278b6bfc1940d02c58016cf09c5c9b1a8f98961ccec75b4097c127c', 0, '2024-09-09 11:19:36'),
('95ed9480e13d49bb0702fa5bf179ad730b5c3845693096c12c97ce8bd088bc098f2661b45a460726', '3fb01daa9ce886229909802bca45e60b403e76d94946efff5db2475c175b66bd6bab14bacc696850', 0, '2024-08-22 10:04:02'),
('96e7e19a7d6bfb555fbc96cd1d943c6ec219ba5dd8b11e4f6e96442028dae7ff0b374a0fa0b1ff4e', '015d4415b34b4740f79ce68f1e4ffd5ef0efa8356a021288483d1d7cc2fb74ee26cd733e20c85e4e', 0, '2024-02-15 12:15:05'),
('98502724bc9701655a4b19a82bdd49750747820e363ca2c96f77d9d42b4e3e4530144d6818c5f9a6', '1c3101712b9cdeb35e5bac418c6a6edde96fd0f9ee7b7b4654c5468063f03614dddf6c557b66194d', 0, '2024-02-15 12:15:04'),
('9b015ea3baac5ec78387413987a3254939667a77b6232b5db903cad2f5e161145b09d67ce100bf04', '29469e4ed44154c84abef7d68cbcf369bec7e833c3b715daf8e9ddd7bc8b84c3630bc180233bff11', 0, '2024-08-05 10:59:35'),
('9beb19f1cd3ea90789c2ad271d34a4d8a1626315dbd3400a576dcb1c0af30b329a8607d7a99b6cd1', '3310541ef8d65e064f4a494aad51431d3eee07f7a0d72703831654784c570163047b64a05d4bd94a', 0, '2024-08-10 16:20:25'),
('9c335aa58a26b1672b63b367e5195f7ac93389adfcd8f2b5edae3c4619f4643454810bb8705298e7', '85e6b52ec93a951cec331337cfd8e12447fab68c661a623fd1c0d4868e36ab68bdeabe8aece9c05e', 0, '2024-02-16 10:52:44'),
('a00693070518e48c597154be49e5e0bc0976b0d766d6c3abe7a9f5923528032d5005273506d8a6cc', '7dbc83d9fb8dd7a97b8664be9f5993a887fb97712de8ddb29f7116da8c73285dc30db8ed160c8e26', 0, '2024-10-02 11:12:10'),
('a0095284d680fc5eeed72b1d82d6de03346a0864ae749af34952aed0225853a5024084348521b235', 'cf9d66976e143ca800f641a2eb2b75eb16d85f00587cf3c199df6c0ccf389892cc6e2beb372abe77', 0, '2024-09-29 15:04:05'),
('a0d24998090d30bfc06ba08f0c887e5b765ca60330d6ed9e2e8a3a380e37f1145d4978f525cfb017', '3fa6f34e592937d0143e1ab5e891a24383530cfb5532244ff22e4cdbea9b84c582934fcbf7ac6029', 0, '2024-06-04 19:46:59'),
('a27a6687d9f5e83d40c254d5ed9725db71d4d65ec6bc6c6a9c52db49169868575c7d6a0f83992f0d', '557cb08b5a4a5b45cbb17f95a51d8b4c8b37a421fdda22b00984de4c3a4b5e601437ad311e09ce13', 0, '2024-08-28 09:26:37'),
('a47fff3b50510a0cfa319d8302fee138b9331d33a575f60e13f9202b6d41fb77065475722950aeb8', 'db677948bfdc57393116edd9c82002c99dbfb997e85d360a710ccb4e78101b441c6828059ba81a8f', 0, '2024-09-27 17:25:14'),
('a6ffe9972c823111d463058c00bfaa39a8eb00c48efc37f4be89016448e9b9ca05ae8d978d9b932c', '8a94e2df1293dc9e7284933c198540f8192c66f342d9d67c54c2f1603bc202e87cd822afbc92d674', 0, '2024-02-15 12:14:55'),
('a8d437125e9d17f7c0403b7f0f2e9dcaf66fda625c743d6a4a90d7e6f6030158fa26e5d152de45d6', '95213c14d496c549e1f45996ecbf1dbf990d4042c2faad1c67e42711227a7b86767dc5804ca26809', 0, '2024-09-27 13:57:17'),
('a9bf9f1b78bf04ad1efa35347f31b342d2ce96434a2dfe123a358ed15f2c4231566d8b95b2959a6c', '080936c6810fe28f2491e66165beb0f39fb64f0d62d21b16003546e0897dd26cb788abf6d3ae0341', 0, '2024-02-15 12:15:03'),
('a9ceb889d45a3863abdbbdccdfa39be4bbb19a99af293adfff86e86471b7560c39e22c194ede99c8', '5af044d19f8d49ccb0bbad4d498ed7f9ffa7cf1ac2487e7db8de6277bf4a5d5a951f8919c07e9564', 0, '2024-02-15 12:15:02'),
('acf6de1586efa698bc5e3208e72a00ace14494c8f1d649d0ca9b26cd1ad40ac70c31d4b14dbb8ffa', '43c99e9688e1c68e7012d18b4625ef0452021a2a78340c3be9f1fe76f02409d5cb3f24d31671f6b9', 0, '2024-06-04 20:52:29'),
('adb11a95904ea02f62a37fa6e1cfafad99051aea309ea43ab386e74184c22fc98facbd7149d07b1c', '5b9507bc0f5efeee50f567983a5c009dd70accf6a1b267b64a2e1c00658ff00f32b43713d17800cd', 0, '2024-06-01 19:41:16'),
('affd5de0f527b134daf48f0d2098ee256f335b111822f9d82bbbd85c9351ff5b834c35bb97e1ba58', 'e814ac44826069bd2c3e48a629881023970b5fea9ff952c9be86ef4b2e325790d3260343438059b4', 0, '2024-10-03 14:02:21'),
('b185f0d467e1fe55a4d04fddb7beee22b23deeba2c2bc68c8498b3035a7d4db332cdcc3504c26a02', 'b8c7571aba4d1abfd60d2277f3ba85a5917d6d57385c230ef578dea3e6c9699fc63b9244dfce3771', 0, '2024-02-16 10:52:43'),
('b1b0003a65eee1960898745117d0140e6ba71c764033650edddc999613d562386029716dbae78e6f', 'c80715d06c9c7be367893ee25a52faccd0e838d8d6705cef13ae301bfdf56f0d8c47a8b628ecc8a6', 0, '2024-08-24 16:12:34'),
('b3bde65cf53a35b6df2b1ca2621d09034d7686aa16d64d341abb3609b146e807ead50198681ed89c', 'dbc60640d04632511118f7edad4c24cea8cb1d68bafc5e6156420cbf60e76b55f819c13c695f379f', 0, '2024-06-04 13:21:39'),
('b5823c145f1bee392d43fc35854f82e2d009e7f08f922a8d1cbf78cefe5900c9ebaefa21b2b5206a', '8cf9831efe57f6a8f681941714666063235e332f35a74a30d461656e321cbadabbd404ed711a13c3', 0, '2024-04-20 17:38:07'),
('b5ba7eb21b0c2dab60f252945c51863106b418d6a57cb20550b8118a881c026233e597b1306a3d46', 'b54cc09aaa4b61b798e2e91dca690152b2110c4d1216ba5755a95363968a69800972f6649210b22b', 0, '2024-06-04 22:07:18'),
('c0209cefc039b5a5c70edf9345bf5ba1baa1786220b4378d0336b9aa384b2036c6f733a10a815b4b', '1cd5f87394913523e799147590bc6e229c8b6a04b960609fc829df2887a9442d66da6a909fe0d040', 0, '2024-08-24 16:13:41'),
('c35258dfb63d084061dc4b7579ab618bca27d4a4e9a5eacba745439345c4970019b026ead01c2bba', '4e504b019c52b5a3d5dcd8783889e62ca54fa74475ea1e0c611a2f7f26cd2c3ea3d3be60cdc461ea', 0, '2024-02-15 12:15:03'),
('c5245e7d952c3aa1634dc7cbbbe039bf6b9b3b3b637db656a487041b7dccf341873f088d15a47fcc', '99627a3a6c66f62e4ab9955f6afb70ff9f7306087ba9b8a84ef10197a3e4adf26f95a6a31e97ae01', 0, '2024-02-15 12:15:04'),
('c5a4337660ddc0c8461bfdd02ef7f8611aaeb41aea3137c1e667ab84da2e2bfe64ac6089ab83427c', 'e95009ad3f63c3c24bb49aad9c1f0d426d37b0facb71188dde8159693ffc7d3f0f79275bd9782271', 0, '2024-08-31 10:26:10'),
('c7169de5c2900ce23c6d89facfbf98249b8a02b17a4b786ea5e1df7653bfa5ad3b4d6524ce93dd5f', '8f803c70354ed38abb5b2a482a218e28ef06050e2d42e710a3af0e54fa87a4d4d851de9258a44360', 0, '2024-06-02 11:11:24'),
('c7efa8860441342dffa01f99b6df9dcda7246d20d5d7bf92d4a8a51f20928d8267ea73f21b910885', 'cf0e009987641a83ef494e4e6d0659c8c1f12b547491da479b3874f372e4a140e6c1c09ee9fc94b1', 0, '2024-02-15 12:15:05'),
('c8deb5d8b6744edb4f2c652b14592415e9a491bf5216196b59aaccffbd33eaed25705734eb0f2d29', '5c23557817cb9504a68af783b6a6d9daf2fdb36010065521b106b8e0ad379147146c4cf6116c9760', 0, '2024-06-01 18:40:26'),
('ca3be7204d63cacce520fe2d0601bc0d9200ed2aee1e0966c4f4de41e68b4a24bbd9e7a021a342c7', 'c709f39aec2935e507357e2d8f59b0b262cb7f9b30db03428fe58248507e1b776be1323d2a0ea3a1', 0, '2024-06-05 08:29:33'),
('ccf89b28ad9fcfbce5b1dc246be8c4d279f3d7b9ee685d1a701fc8f44db62dc482a557d65a3ad9b9', '721949268dbbfaaacf0e89965d4291c60fa37759b106f470af3183917f75b93122fe7b51ae0971e0', 0, '2024-06-03 16:32:22'),
('ceb88cb111c3309d023eab6ae05fb96421d20c575e148d9d291f48bbed4b5a03a933b82ad7d9bf77', 'c4626bc68fecfdf75c18cb0987ee5550e2f304d587d3636ba4e770e59e401be11201dff28a50d7e8', 0, '2024-10-02 14:29:33'),
('d013ddb6810642d6458c43dca0553c5b54fbd52f5d4b825b1b1689509ba8c7150b808e88f6c03344', 'a47a5ea532702bdae63ecc066251ecaa53838519f010b0dd41d1993921b4695d242c238c56ea2c81', 0, '2024-06-03 17:05:12'),
('d1336987f11b7c814a9bac4e9a2b75fe0c3703f949e2691887e4fa72a7b7db578b843fe37d20174c', 'c9c2e016bf1cd7cf4cc478c4ec3915956d3163b7f4115a4d599023bbcf21a3ed1e9b44642546c5c0', 0, '2024-10-02 14:27:47'),
('d564426a93b7051332df6068588d44f90234428d613f5a4708bd856cc44cc8462fcd8e4f26a69a9a', 'd5998595ef08efd07d75a6bc45a5527fb1affde20257f2ca333c2550e5258f423bed8ece4f0fb029', 0, '2024-10-02 09:10:54'),
('d6504c7b94eaa4ba1b45f0f797ade4fa609677ade6bd8893a483d4d17dbf891df5ce02c47fdd826c', '4ab2dc67a42c9742bf30997b5c6f1a20bec953de6a380454a312448488282df680b00eb46d4ebdb9', 0, '2024-08-20 11:53:44'),
('d6c8b7d47fb8e7b6efca9750e70e6662f84d3aee4ff9e88f0aaa00438c39c163a221b61c8f44b8a7', '912c199bffd5c1c6d7c7984a0c68f5365fb4d67243ab6a387138190a52090caef2bf054d61a87622', 0, '2024-02-15 12:15:04'),
('d77dc0f8e0f475af8574784a444f6072e853ce98ba6c484bc50d14bb1350275fa0663f404db6ad62', 'ad6f9619d85c6ba361e7eadd601e09e358cb5a4a65992edac0a38d4f3648dae00c834a9c948090f0', 0, '2024-08-22 10:04:05'),
('ea78955bf76902934d80062186034907cd5114b2e23b647b576222052d21562fedc896a4f990ff7a', 'bdb104f527d468189d494a8cdb4fdfdd3175f53b6fb405c1bb702770137a015ada1e562dcab12568', 0, '2024-08-10 16:06:18'),
('eb060f875ef045f4ed6005baf887eb0b7531a5465a732ee4fbfff42422c9c46ac681be18e4286f54', '91573197ca686ceac0ae5ba68b080144597c0aa2c47daf4ec9daf0749922a77e2938b849dd70b998', 0, '2024-02-15 12:15:04'),
('ecaca4ac0bf993fb2e94c55f4865fe8e55808298555aaa9589472ae9ccf970628dca1f7264335475', '13de714374a509bb86301a7e87e3075a99ab926f6084e51a850e73bb7ad8e312e403b16b4fb401fe', 0, '2024-09-28 17:21:09'),
('ee6d4ad2ba2898c12dee7800655a48a8b7f610cd364a67caaf9b9984284c959debfc0d056f17ecc6', '664e1f238e7eafc698ad4c08d2c5cbc660a2756bd2fb3945fc8566be72dd325cb6caf7ed43fc6c71', 0, '2024-02-16 14:25:31'),
('ef25047851e5be673312b547a570fd06994f3926475fca049cadee103117a7e42b50be4a710391bb', '93144045ea39f1248612c7b38387359e4f6cbdd956444a454d07eb8a736f91a9e4fed8001133db75', 0, '2024-02-23 14:51:09'),
('f010228e2212ba9272926deea78f5081eb78fd865987bf2926f36224e312f4c44ff51998a883a30d', 'ab05f37509c8281bdf73b08bd537b75af1e5b45b985a8b9eb8944457554ab86beebe0af86c8c51c9', 0, '2024-08-07 12:49:18'),
('f153a3fcf5f757ab674078e3489261c9f666ae7d197a486f5fe96825c3027c11ea947741630cba73', 'cca0b4fee59c12b85880d5216c997fb679bf1166a9d3f66f202a36e85126c26ed179804c0281ad5a', 0, '2024-04-20 17:39:29'),
('f4acd7fe740664136df10dde35bbcc70d6deb69b3f85e5f1e8954262d56d35e50a6b061fa4a8e253', '5b8b33a07bafcae6c1bc7fd647be75cb9e959a31f606df8aaad7fb11c7c3429e745fd952b5497f99', 0, '2024-02-15 12:15:05'),
('f59ae6ca651d6037ef5251acaefa6dbf6242bb9512e1afb597598b11408e6e3df291e17e981239b3', '186b2f5f5229ba62b39a96b49c38716c7fefb64d7fc6c5a87ff1fdad6a1d7a23dac3a1321a82f810', 0, '2024-08-07 12:49:00'),
('f7f8820cf539b2a7be3e8dd1e18bf80e6e2d56aabe8a617aadb100e2160a9aae4728ce019489f3da', '4fd8050281ea7f37003e5b128b30bf98341b0d6d6c9040b112aa6f19922b3a937b15468d662fe12b', 0, '2024-08-08 13:46:52'),
('f8e1eb6b81e2c31431acc952ea010aefba71cfdeaa79e9897ffb2cb25f9bf59f343f3cb9cdc89364', '84e9651c739a35d93d4a901428762fb552299294def88d29252c95a2123363debc738e6cbb84ff10', 0, '2024-04-20 17:38:18'),
('fb074cadbb6319eb135a085ebeb3b4c2fdd0a6b0cced661ad3f059db19247b5fb86d410b26982ea7', 'afd01a2ecc5a240ae57cf957539ee3eab5d01dd606c28f2b738494bb886aa8f022c424a51e5a2934', 0, '2024-08-16 18:30:19'),
('fb9e8405071fa39e4dd28e65e321f9a6c36653ba416cdf614af1113ac883f0a0b9d5fa0a9a8e1de4', 'a34f008429a28b4a21610d1c8cfefbdd75f81d06f3cd1ce1856dea5a007fda78abd1f7ed63161f1a', 0, '2024-02-15 12:17:33');

-- --------------------------------------------------------

--
-- Estrutura para tabela `participante_aluno`
--

CREATE TABLE `participante_aluno` (
  `id` int(11) NOT NULL,
  `projeto_id` int(11) DEFAULT NULL,
  `aluno_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `participante_mestrando`
--

CREATE TABLE `participante_mestrando` (
  `id` int(11) NOT NULL,
  `projeto_id` int(11) DEFAULT NULL,
  `mestrando_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `perfil`
--

CREATE TABLE `perfil` (
  `id` int(10) UNSIGNED NOT NULL,
  `perfil` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Despejando dados para a tabela `perfil`
--

INSERT INTO `perfil` (`id`, `perfil`, `created_at`, `updated_at`) VALUES
(1, 'teste1', '2023-02-13 23:01:29', '2023-02-13 23:01:29'),
(2, 'teste2', '2023-08-09 15:03:33', '2023-08-09 15:03:33'),
(3, 'professor', '2023-08-17 19:17:58', '2023-08-17 19:17:58');

-- --------------------------------------------------------

--
-- Estrutura para tabela `perfil_permissoes`
--

CREATE TABLE `perfil_permissoes` (
  `id` int(11) NOT NULL,
  `perfil_id` int(11) DEFAULT NULL,
  `permissao_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Despejando dados para a tabela `perfil_permissoes`
--

INSERT INTO `perfil_permissoes` (`id`, `perfil_id`, `permissao_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2023-02-14 17:09:27', '2023-02-14 17:09:27'),
(120, 1, 2, '2023-08-24 19:10:24', '2023-08-24 19:10:24');

-- --------------------------------------------------------

--
-- Estrutura para tabela `permissoes`
--

CREATE TABLE `permissoes` (
  `id` int(11) NOT NULL,
  `permissao` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `texto` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Despejando dados para a tabela `permissoes`
--

INSERT INTO `permissoes` (`id`, `permissao`, `created_at`, `updated_at`, `texto`) VALUES
(1, 'teste', '2023-02-14 17:08:18', '2023-02-14 17:08:18', 'teste');

-- --------------------------------------------------------

--
-- Estrutura para tabela `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `professores`
--

CREATE TABLE `professores` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Email_Institucional` varchar(100) DEFAULT NULL,
  `Lattes` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `titulacao` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Despejando dados para a tabela `professores`
--

INSERT INTO `professores` (`id`, `name`, `Email`, `Email_Institucional`, `Lattes`, `created_at`, `updated_at`, `titulacao`) VALUES
(1, 'teste', 'teste@gmail.com', 'teste@unimontes', 'teste', NULL, NULL, NULL),
(2, 'Lucas', 'lucas@gmail.com', 'lucas@gmail.com', 'dadad', '2023-06-02 02:12:41', '2023-06-02 02:12:41', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `projetos`
--

CREATE TABLE `projetos` (
  `id` int(11) NOT NULL,
  `tipo` int(11) DEFAULT NULL,
  `resumo` varchar(255) DEFAULT NULL,
  `palavras_chave` varchar(255) DEFAULT NULL,
  `periodo_inicio` timestamp NULL DEFAULT NULL,
  `periodo_finalizacao` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `titulo` int(11) DEFAULT NULL,
  `coordenador` varchar(100) DEFAULT NULL,
  `linhas_pesquisa` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Despejando dados para a tabela `projetos`
--

INSERT INTO `projetos` (`id`, `tipo`, `resumo`, `palavras_chave`, `periodo_inicio`, `periodo_finalizacao`, `created_at`, `updated_at`, `titulo`, `coordenador`, `linhas_pesquisa`) VALUES
(1, 1, 'teste', 'teste', '2023-08-15 12:29:56', '2023-08-15 12:29:56', '2023-08-15 12:29:56', '2023-08-15 12:29:56', 1, 'teste', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `reuniao`
--

CREATE TABLE `reuniao` (
  `id` int(11) NOT NULL,
  `Pauta` varchar(250) DEFAULT NULL,
  `Tipo` tinyint(4) DEFAULT NULL,
  `Link` varchar(250) DEFAULT NULL,
  `Local` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perfil` int(11) NOT NULL DEFAULT 0,
  `foto` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Despejando dados para a tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `username`, `perfil`, `foto`, `type`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$kRkG0BJixbIlTeFsiIQfMeKPmCQgqlZL5ZnJ.3TCCJTZ4/Ie.Z6Ri', NULL, '2023-02-14 16:55:38', '2023-02-14 16:55:38', 'admin', 1, NULL, NULL),
(2, 'iowgvoudsf', 'klnberkn@gmail.com', NULL, '$2y$10$ELBT9f70MUxSs8zHvCzYBe1l4n3s2I8aKhaV9N/BIQAns12cLLsNG', NULL, '2023-02-22 18:57:18', '2023-02-22 18:57:18', 'jefbd', 1, NULL, NULL),
(3, '1', 'jdtym@gmail.com', NULL, '$2y$10$/WndlHPdJ0BF4KwWzRew7e/09O3zSYdFXs/wd0jepeffV6SS9M31O', NULL, '2023-02-23 20:52:00', '2023-02-23 20:52:00', 'htrh', 1, NULL, NULL),
(4, 'oidhwa', 'lucas@gmail.com', NULL, '$2y$10$6RpkREGCxz.JS2YBHRgY8exESRPiaLTryxkZbPrNWHHZJ0m1B5pFy', NULL, '2023-06-02 01:13:03', '2023-06-02 01:13:03', 'Lucas', 2, NULL, NULL),
(5, '2', 'teste@gmail', NULL, '$2y$10$TQvXwzyHVjEbglnTwMHIhOXCCXrwrA7dXtQvxmHxfSS4HYolvadeq', NULL, '2023-08-24 19:08:50', '2023-08-24 19:08:50', 'teste', 2, NULL, NULL),
(6, 'usuarioReserva', 'mail@mail.com', NULL, '$2y$10$hqj3LOJWJsyz6teRyFhWU.VvgmZjB6afntLyRCgJZPUX/0Viei0Zu', NULL, '2023-09-27 22:09:01', '2023-09-27 22:09:01', 'usuarioReserva', 2, NULL, 2),
(12, '5', 'testerota@gmail.com', NULL, '$2y$10$qhAElx29BaguybH7UZHBE.r9BkweHTsCckKdITB5sBca7YM27Tn6W', NULL, '2023-09-28 20:18:24', '2023-09-28 20:18:24', 'teste', 5, NULL, NULL),
(13, '1', '1', NULL, '$2y$10$2sCeBjOpZnRGOru5Hi2.qOvsYI4VGJXhtlEG/d70F8aLX7tnYyw2a', NULL, '2023-09-28 20:18:55', '2023-09-28 20:18:55', '1', 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `websockets_statistics_entries`
--

CREATE TABLE `websockets_statistics_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `peak_connection_count` int(11) NOT NULL,
  `websocket_message_count` int(11) NOT NULL,
  `api_message_count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Índices para tabelas despejadas
--

--
-- Índices de tabela `alunos`
--
ALTER TABLE `alunos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `emprestimomonografia`
--
ALTER TABLE `emprestimomonografia`
  ADD PRIMARY KEY (`id`,`fk_monografia`),
  ADD KEY `fk_EmprestimoMonografia_Monografia1_idx` (`fk_monografia`);

--
-- Índices de tabela `emprestimos`
--
ALTER TABLE `emprestimos`
  ADD PRIMARY KEY (`id`,`fk_equipamento`),
  ADD KEY `fk_Emprestimo_Equipamento1` (`fk_equipamento`),
  ADD KEY `fk_Emprestimo_users1` (`fk_user`);

--
-- Índices de tabela `equipamentos`
--
ALTER TABLE `equipamentos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `mestrandos`
--
ALTER TABLE `mestrandos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `monografias`
--
ALTER TABLE `monografias`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `notasposgraduacao`
--
ALTER TABLE `notasposgraduacao`
  ADD PRIMARY KEY (`id`,`fk_professor`),
  ADD KEY `fk_NotasPosGraduacao_Professor1_idx` (`fk_professor`);

--
-- Índices de tabela `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Índices de tabela `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Índices de tabela `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Índices de tabela `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Índices de tabela `participante_aluno`
--
ALTER TABLE `participante_aluno`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_aluno_id` (`aluno_id`),
  ADD KEY `fk_projeto_id` (`projeto_id`);

--
-- Índices de tabela `participante_mestrando`
--
ALTER TABLE `participante_mestrando`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mestrando_id` (`mestrando_id`),
  ADD KEY `fk_projeto_id` (`projeto_id`);

--
-- Índices de tabela `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `perfil_permissoes`
--
ALTER TABLE `perfil_permissoes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `permissoes`
--
ALTER TABLE `permissoes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Índices de tabela `professores`
--
ALTER TABLE `professores`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `projetos`
--
ALTER TABLE `projetos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `reuniao`
--
ALTER TABLE `reuniao`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Índices de tabela `websockets_statistics_entries`
--
ALTER TABLE `websockets_statistics_entries`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT para tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `emprestimomonografia`
--
ALTER TABLE `emprestimomonografia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `emprestimos`
--
ALTER TABLE `emprestimos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `equipamentos`
--
ALTER TABLE `equipamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT de tabela `monografias`
--
ALTER TABLE `monografias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de tabela `notasposgraduacao`
--
ALTER TABLE `notasposgraduacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de tabela `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `perfil_permissoes`
--
ALTER TABLE `perfil_permissoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT de tabela `permissoes`
--
ALTER TABLE `permissoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de tabela `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `professores`
--
ALTER TABLE `professores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `projetos`
--
ALTER TABLE `projetos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `reuniao`
--
ALTER TABLE `reuniao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Restrições para tabelas despejadas
--

--
-- Restrições para tabelas `emprestimomonografia`
--
ALTER TABLE `emprestimomonografia`
  ADD CONSTRAINT `fk_EmprestimoMonografia_Monografia1` FOREIGN KEY (`fk_monografia`) REFERENCES `monografias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `emprestimos`
--
ALTER TABLE `emprestimos`
  ADD CONSTRAINT `fk_Emprestimo_Equipamento1` FOREIGN KEY (`fk_equipamento`) REFERENCES `equipamentos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Emprestimo_users1` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `notasposgraduacao`
--
ALTER TABLE `notasposgraduacao`
  ADD CONSTRAINT `fk_NotasPosGraduacao_Professor1` FOREIGN KEY (`fk_professor`) REFERENCES `professores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `participante_aluno`
--
ALTER TABLE `participante_aluno`
  ADD CONSTRAINT `fk_aluno_id` FOREIGN KEY (`aluno_id`) REFERENCES `alunos` (`id`),
  ADD CONSTRAINT `fk_projeto_id` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
